# BO-ECLI Parser Engine

The BO-ECLI Parser Engine analyzes the text of legal documents and identifies legal references.

It provides a Java Api and it can be extended in order to add support for different languages and jurisdictions.


### Java Api Usage:

```java
	
	public static void main(String[] args) {
			
		//The BOECLI Engine Document to be used as input for the BOECLI Parser Engine
		BOECLIEngineDocument engineDocument = BOECLIEngineDocumentFactory.getDocument(
				"...dalla Corte Costituzionale con la sentenza n. 51 del 18 febbraio 1992...", 
				"ECLI:IT:CASS:2011:8663CIV", 
				"it");
		
		//Executes the parser through a public static method of the BOECLIEngine class
		if( !BOECLIEngine.run(engineDocument) ) {
			
			System.out.println("Error.");
			
			for(String error : engineDocument.getErrors()) {
				System.err.println(error);
			}
			
		} else {
		
			for(String message : engineDocument.getMessages()) {
				System.out.println(message);
			}
		
			//Print the identified legal references
			System.out.println("\nLegal references: (" + engineDocument.getLegalReferences().size() + ")" );
			
			for(LegalReference reference : engineDocument.getLegalReferences()) {	
				
				System.out.println( "\n" + reference.toString() );
				System.out.println("Context: " + reference.getContext());
				
				//Features
				if(reference.getAliasValue().length() > 0) System.out.println("Alias: " + reference.getAliasValue());
				if(reference.getType().length() > 0) System.out.println("Type of cited document: " + reference.getType());
				if(reference.getSubject().length() > 0) System.out.println("Subject: " + reference.getSubject());
				if(reference.getAuthority().length() > 0) System.out.println("Issued by: " + reference.getAuthority());
				if(reference.getSection().length() > 0) System.out.println("Section: " + reference.getSection());
				if(reference.getDate().length() > 0) System.out.println("Date: " + reference.getDate());
				if(reference.getNumber().length() > 0) System.out.println("Number: " + reference.getNumber(true));
				if(reference.getCaseNumber().length() > 0) System.out.println("Case Number: " + reference.getCaseNumber(true));
				if(reference.getAltNumber().length() > 0) System.out.println("Alternative Number: " + reference.getAltNumber(true));
				if(reference.getApplicant().length() > 0) System.out.println("Applicant: " + reference.getApplicant());
				if(reference.getDefendant().length() > 0) System.out.println("Defendant: " + reference.getDefendant());
				if(reference.getGeographic().length() > 0) System.out.println("Geographic: " + reference.getGeographic());
				if(reference.getMisc().length() > 0) System.out.println("Misc: " + reference.getMisc());
				
				//Partition
				if(reference.getPartition().length() > 0) System.out.println("Partition: " + reference.getPartition());
				
				//Identifiers
				for(LegalIdentifier legalIdentifier : reference.getLegalIdentifiers()) {
						
					String idString = legalIdentifier.getIdentifier().toString() + " identifier: " + legalIdentifier.getCode();
					idString += " [confidence: " + legalIdentifier.getConfidenceValue() + "]";
					
					if(legalIdentifier.getUrl().length() > 0) {
						idString += " - URL: " + legalIdentifier.getUrl();
					}
					
					System.out.println(idString);
				}
			}
			
			System.out.println("\nAnnotated text:\n" + engineDocument.getAnnotatedText());
			System.out.println("\nECLI Reference Metadata:\n" + engineDocument.getECLIReferenceMetadata());
		}
	}
```


