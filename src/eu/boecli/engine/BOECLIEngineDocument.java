/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.engine;

import java.util.Collection;

import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.reference.LegalReference;

public interface BOECLIEngineDocument {

	/*
	 * 
	 * The BOECLIEngineDocument interface defines the methods exposed by an EngineDocument object.
	 * 
	 * A BOECLIEngineDocument object exposes methods for:
	 * - getting the text to be analyzed
	 * - getting the language of the text to be analyzed
	 * - getting the jurisdiction of the text to be analyzed, if set 
	 * - getting the ECLI code of the input text, if set
	 * - getting the results in terms of collections of legal references
	 * - getting the results in terms of annotated text
	 * - providing messages and errors for debug
	 */
	
	public Language getLanguage();	
	public Jurisdiction getJurisdiction();
	public String getInputEcli();
	
	//Utility methods for getting errors and messages after run
	public Collection<String> getErrors();
	public Collection<String> getMessages();

	//A collection of the extracted legal references objects
	public Collection<LegalReference> getLegalReferences();
	
	//The original plain text with inline annotations
	public String getAnnotatedText();

	//Serialized ECLI metadata of the extracted references 
	public String getECLIReferenceMetadata();
}
