/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.engine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.ServiceLoader;
import java.util.TreeSet;

import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.service.BOECLIService;

class ServiceManager {

	/*
	 * Loads and provides the available implementations of BOECLI services.
	 */
	
	//utility collections
	private Collection<Language> supportedLanguages = null;
	private Collection<Jurisdiction> supportedJurisdictions = null;
	
	//Java Service Provider Interface - Service loader
	private ServiceLoader<BOECLIService> boecliServiceLoader = null;
	
	private Collection<BOECLIService> services = null;
	
	//singleton pattern	
	private static ServiceManager serviceManager = null;
	
	public static ServiceManager getInstance() {
		
		if(serviceManager == null) {
			
			serviceManager = new ServiceManager();
		}
		
		return serviceManager;
	}
	
	private ServiceManager() {
		
		supportedLanguages = new TreeSet<Language>();
		supportedJurisdictions = new TreeSet<Jurisdiction>();
		
		services = new TreeSet<BOECLIService>();
		
		initServices();
	}
	
	
	/*
	 * Init every available service implementation once.
	 */
	private void initServices() {
		
		boecliServiceLoader = ServiceLoader.load(BOECLIService.class);
		
		int counter = 0;
		
		for(BOECLIService service : boecliServiceLoader) {
			
			//TODO Syntax check on getVersion() getAuthor() getLanguage() getJurisdiction() etc.
			//if any of these values is null or empty, send a warning and don't load the service.
			
			if(BOECLIEngine.DEBUG) System.out.println("Loading Service " + service.getDescription());
			
			if( !service.language().equals(Language.DEFAULT)) {
			
				supportedLanguages.add(service.language());
			}
			
			if( !service.jurisdiction().equals(Jurisdiction.DEFAULT)) {
				
				supportedJurisdictions.add(service.jurisdiction());
			}
			
			counter++;
			service.setIndex(counter);
			
			services.add(service);
		}
	}
	
	/*
	 * Returns a fixed-order collection of the available services in a given language 
	 * and jurisdiction (if specified), along with the available default services.
	 */
	public Collection<BOECLIService> getServices(Language language, Jurisdiction jurisdiction) {

		Collection<BOECLIService> selectedServices = new ArrayList<BOECLIService>();

		for(BOECLIService service : services) {
			
			if(service.language().equals(language) &&
					( jurisdiction == null || service.jurisdiction().equals(jurisdiction) ) ) {

				selectedServices.add(service);
				continue;
			}
			
			if(service.language().equals(Language.DEFAULT)) {
				
				selectedServices.add(service);
				continue;
			}
		}
		
		return selectedServices;
	}
}

