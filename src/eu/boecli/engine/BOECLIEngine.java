/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.engine;

import eu.boecli.service.AnnotationService;
import eu.boecli.service.BOECLIService;

public final class BOECLIEngine {

	/*
	 * The BOECLIEngine class exposes static methods  
	 * for running the extraction process on a BOECLIEngineDocument
	 */
	
	public final static String VERSION = "1.0.3";
	
	public final static boolean DEBUG = false;
	
	/**
	 * Executes the BOECLI Engine parsing process through a pipeline of BOECLI services.
	 * 
	 * @param engineDocument
	 * @return false in case of errors
	 */
	public static boolean run(BOECLIEngineDocument engineDocument) {
		
		if(DEBUG) System.out.println("Running BOECLI Parser Engine (ver. " + VERSION + ")...\n");
		((EngineDocument) engineDocument).addMessage("Running BOECLI Parser Engine (ver." + VERSION + ")...\n");
		
		if(engineDocument.getLanguage() == null) {
			
			((EngineDocument) engineDocument).addError("Language of the input not set or not recognized (null).");
			return false;
		}
		
		boolean errors = false;
		
		//Run every service implementation for the specified language/jurisdiction
		for(BOECLIService service : ServiceManager.getInstance().getServices(engineDocument.getLanguage(), engineDocument.getJurisdiction())) {
			
			//Initialize the service
			if( !service.init(engineDocument)) {
				
				((EngineDocument) engineDocument).addError("Service initialization failed. (" + service.getDescription() + ")");
				errors = true;
				continue;
			}
			
			//Check if the service is runnable on this input
			if( !service.runnable()) {
				continue;
			}
			
			//Run the service
			if( !service.runService()) {
				
				//Manage failures
				((EngineDocument) engineDocument).addError("Service failed. (" + service.getDescription() + ")");
				errors = true;
				break; //A generic failure of a service does or doesn't stop the pipeline ?
				
			} else {
			
				if(service instanceof AnnotationService) {
					
					//Update the annotation history
					((EngineDocument) engineDocument).addAnnotation((AnnotationService) service);
				}
			}
		}
		
		if(DEBUG) {
			
			//Print the annotation history
			System.out.println("\n\nANNOTATIONS: \n");
			
			int count = 1;
			for(AnnotationService service : ((EngineDocument) engineDocument).getAnnotations() ) {
				
				System.out.println("\n" + count + ") " + service.getDescription());
				System.out.println("BEFORE: " + service.getBefore());
				System.out.println("AFTER: " + service.getAfter());
				count++;
			}
		}
		
		return !errors;
	}
}
