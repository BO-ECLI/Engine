/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.engine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import eu.boecli.common.CommonIdentifiers;
import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.reference.LegalIdentifier;
import eu.boecli.reference.LegalReference;
import eu.boecli.service.AnnotationService;

public final class EngineDocument implements BOECLIEngineDocument {

	/*
	 * 
	 * Actual implementation of a BOECLIEngineDocument.
	 * 
	 * To be used as input for a BOECLIEngine run.
	 * 
	 */
	
	
	//Original Input text
	private String originalText = "";
	
	private String plainText = "";


	//Error and warning captured during the execution of the Engine
	private Collection<String> errors = null;
	private Collection<String> messages = null;

	private Collection<LegalReference> legalReferences = null;
	
	private Map<String,LegalReference> id2legalReference = null;
	
	private Collection<AnnotationService> annotations = null;

	//Metadata of the input document
	private Language language = null;
	private Jurisdiction jurisdiction = null;

	private String inputEcli = "";
	private String inputAuthority = ""; //Combines the 2nd and 3rd field of the ECLI code, separated with an underscore
	private String inputYear = ""; //The 4th field of the ECLI code
	private String inputOrdinalNumber = ""; //The 5th field of the ECLI code

	
	//Use the Factory for instantiating a BOECLIEngineDocument
	EngineDocument() {
		
		init();
	}
	
	private void init() {
		
		//init or reset the legal references collection
		legalReferences = new TreeSet<LegalReference>();

		id2legalReference = new HashMap<String,LegalReference>();
		
		annotations = new ArrayList<AnnotationService>();
		
		errors = new ArrayList<String>();
		
		messages = new ArrayList<String>();
	}
	
	@Override
	public final String getECLIReferenceMetadata() {

		//Produce the ECLI Reference Metadata for the extracted references and return them as a String
		//<reference lang="it" relation="citing" type="ECLI">ECLI:IT:COST:1992:S51</reference>
		
		String metadata = "";
		
		String lang = getLanguage().toString().toLowerCase();
		
		Set<String> codes = new HashSet<String>();
		
		for(LegalReference legalReference : getLegalReferences()) {
			
			Set<String> types = new HashSet<String>();
			
			for(LegalIdentifier legalIdentifier : legalReference.getLegalIdentifiers()) {

				String type = "OTHER";
				
				if(legalIdentifier.getIdentifier().equals(CommonIdentifiers.ECLI)) type = "ECLI";
				if(legalIdentifier.getIdentifier().equals(CommonIdentifiers.ELI)) type = "ELI";
				if(legalIdentifier.getIdentifier().equals(CommonIdentifiers.CELEX)) type = "CELEX";

				//One identifier (max score) for each type
				if(types.contains(type)) continue;
				types.add(type);
				
				String code = legalIdentifier.getCode();
				
				//Distinct output
				if(codes.contains(code)) continue;
				codes.add(code);
				
				String entry = "<reference lang=\"" + lang + "\" relation=\"citing\" type=\"" + type + "\">"
								+ code + "</reference>";
				
				metadata += entry + "\n";
			}
		}
		
		return metadata;
	}
	
	/*
	 * Debug purposes only.
	 */
	public String getBoecliAnnotations() {

		if(annotations.size() < 4) {
			
			return this.plainText;
		}
		
		return ((ArrayList<AnnotationService>) annotations).get(annotations.size() - 3).getAfter();
	}
	
	public boolean addLegalReference(LegalReference legalReference) {
		
		boolean res =  this.legalReferences.add(legalReference);
		
		if(res) {
			
			id2legalReference.put(String.valueOf(legalReference.getId()), legalReference);
		}
		
		return res;
	}	
	
	public LegalReference getLegalReference(String id) {
		
		return id2legalReference.get(id);
	}
	
	public Collection<AnnotationService> getAnnotations() {
		
		return Collections.unmodifiableCollection(this.annotations);
	}
	
	public boolean addAnnotation(AnnotationService service) {
		
		return this.annotations.add(service);
	}

	void setPlainText(String plainText) {
		
		//TODO
		this.originalText = plainText;
		
		preProcessing();
	}
	
	@Override
	public final String getAnnotatedText() {

		/*
		 * The original text annotated in a specific format.
		 */
		
		if(annotations.size() == 0) {
			
			return this.plainText;
		}
		
		return ((ArrayList<AnnotationService>) annotations).get(annotations.size() - 1).getAfter();
	}

	@Override
	public final Collection<LegalReference> getLegalReferences() {
		
		/*
		 * A collection of the extracted legal references.
		 */

		return Collections.unmodifiableCollection(legalReferences);
	}
	
	@Override
	public final Collection<String> getErrors() {

		return Collections.unmodifiableCollection(errors);
	}

	@Override
	public final Collection<String> getMessages() {

		return Collections.unmodifiableCollection(messages);
	}

	public void addError(String error) {
		
		errors.add(error);
	}
	
	public void addMessage(String message) {
		
		messages.add(message);
	}
	
	/*
	 * Gets the internal annotations (debug purposes)
	 */
	public String debugAnnotations() {
		
		return getAnnotatedText();
	}
	
	/*
	 * preProcessing e postProcessing of plain text.
	 * TODO 
	 */
	private void preProcessing() {
	
		plainText = originalText;
		
		//TODO spaces are needed if the text completely coincides with a pattern of a legal reference. postProcessing() should remove them after.
		
		plainText = " " + plainText + " ";

	}
	
	// TODO
	/*
	private void postProcessing() {
		
		if(annotatedText.length() > 1 && 
				annotatedText.substring(annotatedText.length()-1, annotatedText.length()).equals(" ")) {
			
			annotatedText = annotatedText.substring(0, annotatedText.length()-1);
		}		
	}
	*/

	//The language of the text (two-letter codes - ISO 639-1)
	void setLanguage(Language language) {

		if(language != null) {
			
			this.language = language;
		}
	}

	//The jurisdiction of the authority that issued the text (two letters country codes - ISO 3166-1 alpha-2 codes)
	void setJurisdiction(Jurisdiction jurisdiction) {

		if(jurisdiction != null) {
			
			this.jurisdiction = jurisdiction;
		}
	}
	
	@Override
	public final Language getLanguage() {
		
		return language;
	}

	@Override
	public final Jurisdiction getJurisdiction() {
		
		return jurisdiction;
	}

	void setInputEcli(String ecli) {

		if(ecli == null) {
			
			return;
		}
		
		this.inputEcli = ecli;
		
		String[] items = ecli.split(":");
		if(items.length != 4 && items.length != 5) {
			return;
		}
		
		int i = 0;
		if(items.length == 5) {
			i = 1;
		}
		
		String country = items[i].trim().toUpperCase();
		String auth = items[i+1].trim().toUpperCase();
		String year = items[i+2].trim().toUpperCase();
		String ordNum = items[i+3].trim().toUpperCase();
		
		setInputAuthority(country + "_" + auth);
		setInputYear(year);
		setInputOrdinalNumber(ordNum);
	}
	
	@Override
	public final String getInputEcli() {
		return inputEcli;
	}
	
	public String getInputAuthority() {
		return inputAuthority;
	}

	private void setInputAuthority(String inputAuthority) {
		this.inputAuthority = inputAuthority;
	}

	public String getInputYear() {
		return inputYear;
	}

	private void setInputYear(String inputYear) {
		this.inputYear = inputYear;
	}

	public String getInputOrdinalNumber() {
		return inputOrdinalNumber;
	}

	private void setInputOrdinalNumber(String inputOrdinalNumber) {
		this.inputOrdinalNumber = inputOrdinalNumber;
	}

}
