/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.engine;

import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;

public class BOECLIEngineDocumentFactory {

	/*
	 * The factory exposes static methods to instantiate BOECLIEngineDocument objects
	 * based on a plain input text, a langauge, an optional jurisdiction and an optional ECLI code.
	 */
	
	/**
	 * 
	 * @param inputText
	 * @param language
	 * @return
	 */
	public static BOECLIEngineDocument getDocument(String inputText, String language) {
		
		Language lang = null;
		
		try {
		
			lang = Language.valueOf(language.trim().toUpperCase());

		} catch (Exception e) {

			lang = null;
		}
		
		return getDocument(inputText, lang);
	}
	
	/**
	 * 
	 * @param inputText
	 * @param inputEcli
	 * @param language
	 * @return
	 */
	public static BOECLIEngineDocument getDocument(String inputText, String inputEcli, String language) {
		
		Language lang = null;
		
		try {
		
			lang = Language.valueOf(language.trim().toUpperCase());

		} catch (Exception e) {

			lang = null;
		}
		
		return getDocument(inputText, inputEcli, lang);
	}
	
	/**
	 * 
	 * @param inputText
	 * @param inputEcli
	 * @param language
	 * @param jurisdiction
	 * @return
	 */
	public static BOECLIEngineDocument getDocument(String inputText, String inputEcli, String language, String jurisdiction) {
		
		Language lang = null;
		
		try {
		
			lang = Language.valueOf(language.trim().toUpperCase());

		} catch (Exception e) {

			lang = null;
		}
		
		Jurisdiction jur = null;
		
		try {
		
			jur = Jurisdiction.valueOf(jurisdiction.trim().toUpperCase());

		} catch (Exception e) {

			jur = null;
		}
		
		return getDocument(inputText, inputEcli, lang, jur);
	}
	
	/**
	 * 
	 * @param inputText
	 * @param language
	 * @return
	 */
	public static BOECLIEngineDocument getDocument(String inputText, Language language) {
		
		return getDocument(inputText, null, language, null);
	}
	
	/**
	 * 
	 * @param inputText
	 * @param inputEcli
	 * @param language
	 * @return
	 */
	public static BOECLIEngineDocument getDocument(String inputText, String inputEcli, Language language) {
		
		return getDocument(inputText, inputEcli, language, null);
	}
	
	/**
	 * 
	 * @param inputText
	 * @param language
	 * @param jurisdiction
	 * @return
	 */
	public static BOECLIEngineDocument getDocument(String inputText, Language language, Jurisdiction jurisdiction) {
		
		return getDocument(inputText, null, language, jurisdiction);
	}
	
	/**
	 * 
	 * @param inputText
	 * @param inputEcli
	 * @param language
	 * @param jurisdiction
	 * @return
	 */
	public static BOECLIEngineDocument getDocument(String inputText, String inputEcli, Language language, Jurisdiction jurisdiction) {

		BOECLIEngineDocument engineDocument = new EngineDocument();
		
		if(language != null) {
		
			((EngineDocument) engineDocument).setLanguage(language);
		}
		
		if(jurisdiction != null) {
		
			((EngineDocument) engineDocument).setJurisdiction(jurisdiction);
		}
		
		if(inputText != null) {
		
			((EngineDocument) engineDocument).setPlainText(inputText);
		}
		
		if(inputEcli != null) {
			
			((EngineDocument) engineDocument).setInputEcli(inputEcli);
		}
		
		return engineDocument;
	}
	
}
