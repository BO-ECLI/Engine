/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Tommaso Agnoloni (ITTIG/CNR), Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.common;

public enum CommonLegislationAliases implements LegislationAlias {

	

	/* TREATIES */
	
	
	TREATY_EU,
	
	TREATY_FUNCTIONING_EU,
	
	TREATY_EU_ATOMIC_ENERGY_COMMUNITY,
	
	TREATY_LISBON,
	
	TREATY_EU_COMMUNITY,
	
	MERGER_TREATY,
	
	TREATY_EEC,
	
	TREATY_EU_COAL_STEEL,
	
	TREATY_EU_CONSTITUTION,
	
	TREATY_NICE,
	
	TREATY_AMSTERDAM, 
	
	TREATY_MAASTRICHT,
	
	EU_CHARTER;

	
}

