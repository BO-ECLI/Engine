/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.common;

public enum CommonCaseLawAliases implements CaseLawAlias {


	
	/* LANDMARK COURT DECISIONS */
	

	
	/*
	 * 
	 * https://en.wikipedia.org/wiki/List_of_European_Court_of_Justice_rulings
	 * 

Costa v ENEL 6/64 [1964] ECR 585
Community law takes precedence over the Member States own domestic law.

Simmenthal II 106/77 [1978] ECR 629
Duty to set aside provisions of national law which are incompatible with Community law.

Marleasing C-106/89 ECR I-7321
National law must be interpreted and applied, insofar as possible, so as to avoid a conflict with a Community rule.

Factortame I C-213/89 [1990] ECR I-2433
Duty on national courts to secure the full effectiveness of Community law, even where it is necessary to create a national remedy where none had previously existed.

Van Gend en Loos 26/62 [1963] ECR 1

Van Gend en Loos v Nederlandse Administratie der Belastingen (1963) Case 26/62 was a landmark case of the European Court of Justice which established that provisions of the Treaty Establishing the European Economic Community were capable 


	 */
	
	COSTA_ENEL,
	
	VAN_GEND_EN_LOOS;
	

}
