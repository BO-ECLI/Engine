/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.common;

public enum Entity {

	/*
	 * List of annotation entities.
	 * 
	 * 
	 * 
	 * Comments provide the supported usage of each entity.
	 * 
	 * Uppercase VALUE means a normalized value from a controlled list
	 * (extensible and non-extensible lists).
	 * 
	 * Lowercase value means a free String. A syntax check is performed
	 * on these values depending on the nature of the entity
	 * (e.g. value for the entity YEAR is free String, but it is expected
	 * to be a 4-digits String representing a number in the range 1921-2020).
	 * 
	 * The value of DATE should be normalized in the following format: yyyy-mm-dd
	 * 
	 * Partitions (like articles, letters, etc.) are expected to have a value
	 * (usually a number or a letter) and, optionally, a Latin descriptor (like "bis"). 
	 * 
	 * 
	 */
	
	IDENTIFIER, //[BOECLI:IDENTIFIER:IDENTIFIER_TYPE]
	
	CASELAW_ALIAS, //[BOECLI:CASELAW_ALIAS:ALIAS_VALUE]
	LEGISLATION_ALIAS,  //[BOECLI:LEGISLATION_ALIAS:ALIAS_VALUE]
	
	CASELAW_AUTHORITY, //[BOECLI:CASELAW_AUTHORITY:CASELAW_AUTHORITY_VALUE]
	LEGISLATION_AUTHORITY, //[BOECLI:LEGISLATION_AUTHORITY:LEGISLATION_AUTHORITY_VALUE]

	SECTION, //[BOECLI:SECTION:Value]
	
	CASELAW_TYPE, //[BOECLI:CASELAW_TYPE:CASELAW_TYPE_VALUE]
	LEGISLATION_TYPE, //[BOECLI:LEGISLATION_TYPE:LEGISLATION_TYPE_VALUE]
	LEGAL_TYPE, //[BOECLI:LEGAL_TYPE:LEGAL_TYPE_VALUE]
	
	SUBJECT, //[BOECLI:SUBJECT:SUBJECT_VALUE]
	
	APPLICANT,  //first party (or unique party)
	DEFENDANT, //second party
	VERSUS, //applicant-defendant separator
	
	DATE, //[BOECLI:DATE:YYYY-MM-DD]

	NUMBER, //[BOECLI:NUMBER:Value]
	CASE_NUMBER,  //[BOECLI:CASE_NUMBER:Value]  
	ALT_NUMBER,  //[BOECLI:ALT_NUMBER:Value]
	
	PARTITION, //TODO a single hierarchical branch of a reference to a specific partition
	ARTICLE, //[BOECLI:ARTICLE:Value:Latin]
	COMMA, //[BOECLI:COMMA:Value:Latin]
	PARAGRAPH, //[BOECLI:PARAGRAPH:Value:Latin]
	LETTER, //[BOECLI:LETTER:Value:Latin]
	ITEM,  //[BOECLI:ITEM:Value:Latin] //Numbered item within a letter
	
	GEO, //[BOECLI:GEO] or [BOECLI:GEO:Value]
	
	MISC, //[BOECLI:MISC]  //For the annotation of further or generic informations about the reference
	
	IGNORE, /* Annotates a false positive to reduce potential ambiguities. */
	STOPWORD, /* Annotates stopwords. Used as allowed separators in order to not interrupt a pattern of reference recognition.  */
	
	LEGAL_REFERENCE //[BOECLI:LEGAL_REFERENCE:ID] or [BOECLI:LEGAL_REFERENCE:ID:MID]
	
}
