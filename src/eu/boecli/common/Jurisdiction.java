/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Tommaso Agnoloni (ITTIG/CNR), Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.common;

public enum Jurisdiction {
	
	DEFAULT, //DEFAULT
	
	
	AT, //Austria
	BE, //Belgium
	BG, //Bulgaria
	HR, //Croatia
	CY, //Cyprus
	CZ, //Czechia
	DK, //Denmark
	EE, //Estonia
	FI, //Finland
	FR, //France
	DE, //Germany
	GR, //Greece
	HU, //Hungary
	IE, //Ireland
	IT, //Italy
	LV, //Latvia
	LT, //Lithuania
	LU, //Luxembourg
	MT, //Malta
	NL, //Netherlands
	PL, //Poland
	PT, //Portugal
	RO, //Romania
	SK,  //Slovakia
	SI, //Slovenia
	ES, //Spain
	SE, //Sweden
	UK  //United Kingdom
	
}
