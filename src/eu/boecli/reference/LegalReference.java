/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.reference;

import java.util.Collection;
import java.util.TreeSet;

import eu.boecli.common.Identifier;
import eu.boecli.engine.BOECLIEngine;
import eu.boecli.service.impl.Util;

public final class LegalReference implements Comparable<LegalReference> {

	
	//Each legal reference can have multiple identifiers of various kind, assigned by different modules (different provenance)
	private Collection<LegalIdentifier> legalIdentifiers = new TreeSet<LegalIdentifier>();
	
	//Self-assigned univocal id within a single run of the software
	private int id = 0;
	
	//text of the citation (or part of) that strictly relates to this specific legal reference
	private String text = "";
	
	//if present, it is a bigger portion of text that includes 'text'. Useful with multiple citations or scattered features.
	private String context = "";
	
	//info about the module/service that produced this LegalReference
	private String provenance = "";
	
	//cited partition within the cited document	
	private String partition = "";

	//issuing authority of the cited document
	private String authority = "";
	
	//type of cited document
	private String type = ""; 

	//issuing authority section of the cited document
	private String section = "";

	//subject of the cited document (civil law, criminal law, etc.)
	private String subject = "";

	//date of the cited document (yyyy-mm-dd)
	private String date = "";
	
	//number
	private String fullNumber = "";
	private String number = "";
	private String year = "";
	private String numberPrefix = "";
	
	//case number 
	private String fullCaseNumber = "";
	private String caseNumber = "";
	private String caseYear = "";
	private String casePrefix = "";

	//alternative number
	private String fullAltNumber = "";
	private String altNumber = "";
	private String altYear = "";
	private String altPrefix = "";
	
	private String applicant = "";
	
	private String defendant = "";
	
	//Geographic information
	private String geographic = "";
	
	//Further information
	private String misc = "";
	
	/*
	 * Normalized value of the alias (used in the annotation).
	 * Could be a value from a national extension.
	 * The lookup to an Alias object is postponed to the
	 * identifier generation services for aliases.  
	 */
	private String aliasValue = "";

	private String ecliIdentifierFeature = "";	
	
	
	private boolean caseLawReference = false;
	private boolean legislationReference = false;


	//Prevent instantiations without Factory
	LegalReference() { }
	

	//Add a LegalIdentifier to this LegalReference
	public boolean addIdentifier(LegalIdentifier legalIdentifier) {
		
		if(legalIdentifier == null || legalIdentifier.getCode().length() < 1) {
			return false;
		}
		
		Identifier identifier = legalIdentifier.getIdentifier();
		
		if(identifier == null) {
			return false;
		}
		
		return legalIdentifiers.add(legalIdentifier);
	}


	//Returns the collection of the LegalIdentifier objects sorted by confidence
	public Collection<LegalIdentifier> getLegalIdentifiers() {
		
		return legalIdentifiers;
	}
	
	/*
	 * TODO
	 * introduce confidence as a value of a legalReference to be used for more shallow extractions
	 */
	public int compareTo(LegalReference o) {

		if(this.getId() < o.getId()) return -1;
		
		if(this.getId() > o.getId()) return 1;

		return this.toString().compareTo(o.toString());
	}
	
	public int getId() {
		return id;
	}

	void setId(int id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}
	
	public String getContext() {
		return context;
	}
	
	public void setContext(String context) {
		this.context = context;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getProvenance() {
		
		if( !BOECLIEngine.DEBUG) {
			
			return "";
		}
		
		return provenance;
	}

	public void setProvenance(String provenance) {
		this.provenance = provenance;
	}

	public boolean isCaseLawReference() {
		return caseLawReference;
	}

	public void setCaseLawReference(boolean caseLawReference) {
		this.caseLawReference = caseLawReference;
	}

	public boolean isLegislationReference() {
		return legislationReference;
	}

	public void setLegislationReference(boolean legislationReference) {
		this.legislationReference = legislationReference;
	}

	public String getPartition() {
		return partition;
	}

	public void setPartition(String partition) {
		this.partition = partition;
	}
	

	/**
	 * Deprecated. Use getApplicant() instead.
	 * 
	 * @return
	 */
	@Deprecated
	public String getPlaintiff() {
		return applicant;
	}

	public String toString() {
		
		return "LegalReference [id:" + getId() + "] (\"" + getText() + "\")";
	}
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getSection() {
		
		if(section == null) return "";
		
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getAuthority() {
		
		if(authority == null) return "";
		
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public String getType() {
		
		if(type == null) return "";
		
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSubject() {
		
		if(subject == null) return "";
		
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getApplicant() {
		
		if(applicant == null) return "";
		
		return applicant;
	}

	public void setApplicant(String applicant) {
		this.applicant = applicant;
	}

	public String getDefendant() {
		
		if(defendant == null) return "";
		
		return defendant;
	}

	public void setDefendant(String defendant) {
		this.defendant = defendant;
	}

	public String getGeographic() {
		return geographic;
	}

	public void setGeographic(String geographic) {
		this.geographic = geographic;
	}
	
	public String getAliasValue() {
		
		if(aliasValue == null) return "";
		
		return aliasValue;
	}

	public void setAliasValue(String aliasValue) {
		this.aliasValue = aliasValue;
	}

	public String getEcliIdentifierFeature() {
		
		if(ecliIdentifierFeature == null) return "";
		
		return ecliIdentifierFeature;
	}

	public void setEcliIdentifierFeature(String ecliIdentifierFeature) {
		this.ecliIdentifierFeature = ecliIdentifierFeature;
	}

	public String getMisc() {
		return misc;
	}

	public void setMisc(String misc) {
		this.misc = misc;
	}

	public String getNumber() {
		
		return getNumber(false);
	}
	
	public String getNumber(boolean full) {
		
		if(full) {
			
			return fullNumber;
		}
		
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getYear() {
		
		return Util.normalizeYear(year);
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getNumberPrefix() {
		return numberPrefix;
	}

	public void setNumberPrefix(String numberPrefix) {
		this.numberPrefix = numberPrefix;
	}

	public String getCaseNumber() {
		
		return getCaseNumber(false);
	}
	
	public String getCaseNumber(boolean full) {
		
		if(full) {
			
			return fullCaseNumber;
		}
		
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public String getCaseYear() {
		return Util.normalizeYear(caseYear);
	}

	public void setCaseYear(String caseYear) {
		this.caseYear = caseYear;
	}

	public String getCasePrefix() {
		return casePrefix;
	}

	public void setCasePrefix(String casePrefix) {
		this.casePrefix = casePrefix;
	}

	public String getAltNumber() {
		
		return getAltNumber(false);
	}
	
	public String getAltNumber(boolean full) {
		
		if(full) {
			
			return fullAltNumber;
		}
		
		return altNumber;
	}

	public void setAltNumber(String altNumber) {
		this.altNumber = altNumber;
	}

	public String getAltYear() {
		
		return Util.normalizeYear(altYear);
	}

	public void setAltYear(String altYear) {
		this.altYear = altYear;
	}

	public String getAltPrefix() {
		return altPrefix;
	}

	public void setAltPrefix(String altPrefix) {
		this.altPrefix = altPrefix;
	}

	public void setFullNumber(String fullNumber) {
		this.fullNumber = fullNumber;
	}

	public void setFullCaseNumber(String fullCaseNumber) {
		this.fullCaseNumber = fullCaseNumber;
	}

	public void setFullAltNumber(String fullAltNumber) {
		this.fullAltNumber = fullAltNumber;
	}

}
