/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.reference;

import java.util.Map;

import eu.boecli.common.CommonIdentifiers;
import eu.boecli.common.Entity;
import eu.boecli.engine.BOECLIEngineDocument;
import eu.boecli.engine.EngineDocument;

public final class LegalReferenceFactory {

	
	public static LegalReference cloneLegalReference(LegalReference legalReference, BOECLIEngineDocument engineDocument) {

		LegalReference newLegalReference = new LegalReference();
		
		newLegalReference.setText(legalReference.getText());
		newLegalReference.setContext(legalReference.getContext());
		
		newLegalReference.setProvenance(legalReference.getProvenance());
		newLegalReference.setPartition(legalReference.getPartition());
		newLegalReference.setAuthority(legalReference.getAuthority());
		newLegalReference.setType(legalReference.getType());
		newLegalReference.setSection(legalReference.getSection());
		newLegalReference.setSubject(legalReference.getSubject());
		newLegalReference.setDate(legalReference.getDate());
		newLegalReference.setFullNumber(legalReference.getNumber(true));
		newLegalReference.setNumber(legalReference.getNumber());
		newLegalReference.setYear(legalReference.getYear());
		newLegalReference.setNumberPrefix(legalReference.getNumberPrefix());
		newLegalReference.setFullCaseNumber(legalReference.getCaseNumber(true));
		newLegalReference.setCaseNumber(legalReference.getCaseNumber());
		newLegalReference.setCaseYear(legalReference.getCaseYear());
		newLegalReference.setCasePrefix(legalReference.getCasePrefix());
		newLegalReference.setFullAltNumber(legalReference.getAltNumber(true));
		newLegalReference.setAltNumber(legalReference.getAltNumber());
		newLegalReference.setAltYear(legalReference.getAltYear());
		newLegalReference.setAltPrefix(legalReference.getAltPrefix());
		newLegalReference.setApplicant(legalReference.getApplicant());
		newLegalReference.setDefendant(legalReference.getDefendant());
		newLegalReference.setGeographic(legalReference.getGeographic());
		newLegalReference.setMisc(legalReference.getMisc());
		newLegalReference.setAliasValue(legalReference.getAliasValue());
		newLegalReference.setEcliIdentifierFeature(legalReference.getEcliIdentifierFeature());
		newLegalReference.setCaseLawReference(legalReference.isCaseLawReference());
		newLegalReference.setLegislationReference(legalReference.isLegislationReference());
		
		//Generate and assign an id for the legal reference
		newLegalReference.setId(engineDocument.getLegalReferences().size() + 1);
		
		//Add the newly created legal reference to the legal references collection
		((EngineDocument) engineDocument).addLegalReference(newLegalReference);
		
		return newLegalReference;
	}
	
	
	/*
	 * Instantiate and return a LegalReference object.
	 */
	public static LegalReference createLegalReference(Map<Entity,String> textFeatures, 
						Map<Entity,String> contextFeatures, Map<Entity,String> customFeatures,
						BOECLIEngineDocument engineDocument) {
		

		//Read the features of the reference
		
		LegalReference newLegalReference = new LegalReference();

		if(customFeatures != null) {

			readFeatures(newLegalReference, customFeatures);
		}

		readFeatures(newLegalReference, textFeatures);

		if(contextFeatures != null) {
			
			readContextFeatures(newLegalReference, contextFeatures);
		}
		
		//Generate and assign an id for the legal reference
		newLegalReference.setId(engineDocument.getLegalReferences().size() + 1);
		
		//Add the newly created legal reference to the legal references collection
		((EngineDocument) engineDocument).addLegalReference(newLegalReference);
		
		return newLegalReference;
	}

	private static boolean readFeatures(LegalReference reference, Map<Entity,String> features) {
		
		readIdentifier(reference, features);
		readLegislationAlias(reference, features);
		readCaseLawAlias(reference, features);
		readType(reference, features);
		readAuthority(reference, features);
		readSubject(reference, features);
		readSection(reference, features);
		readDate(reference, features);
		readPartition(reference, features);
		readNumber(reference, features);
		readCaseNumber(reference, features);
		readAltNumber(reference, features);
		readApplicant(reference, features);
		readDefendant(reference, features);
		readGeo(reference, features);
		readMisc(reference, features);
		
		return true;
	}

	private static boolean readContextFeatures(LegalReference reference, Map<Entity,String> features) {
		
		readLegislationAlias(reference, features);
		readCaseLawAlias(reference, features);
		readType(reference, features);
		readAuthority(reference, features);
		readSubject(reference, features);
		readSection(reference, features);
		
		return true;
	}
	
	private static boolean readIdentifier(LegalReference reference, Map<Entity,String> features) {
		
		if(reference.getEcliIdentifierFeature() != null && reference.getEcliIdentifierFeature().length() > 0) {
			return false;
		}

		String value = features.get(Entity.IDENTIFIER);
		
		if(value != null) {
			
			reference.setEcliIdentifierFeature(value);
			
			if(value.equalsIgnoreCase(CommonIdentifiers.ECLI.toString())) {
				
				reference.setCaseLawReference(true);
			}

			return true;
		}
	
		return false;
	}
		
	private static boolean readLegislationAlias(LegalReference reference, Map<Entity,String> features) {
		
		if(reference.getAliasValue() != null && reference.getAliasValue().length() > 0) {
			return false;
		}

		String value = features.get(Entity.LEGISLATION_ALIAS);
		
		if(value != null) {
			
			reference.setAliasValue(value);
			
			reference.setLegislationReference(true);

			return true;
		}
	
		return false;
	}
	
	
	private static boolean readCaseLawAlias(LegalReference reference, Map<Entity,String> features) {
		
		if(reference.getAliasValue() != null && reference.getAliasValue().length() > 0) {
			return false;
		}

		String value = features.get(Entity.CASELAW_ALIAS);
		
		if(value != null) {
			
			reference.setAliasValue(value);
			
			reference.setCaseLawReference(true);

			return true;
		}
	
		return false;
	}
	
	private static boolean readType(LegalReference reference, Map<Entity,String> features) {
		
		if(reference.getType() != null && reference.getType().length() > 0) {
			return false;
		}
		
		String value = features.get(Entity.LEGISLATION_TYPE);
		
		if(value != null) {
			
			reference.setType(value);
			reference.setLegislationReference(true);
			return true;
		}
		
		value = features.get(Entity.CASELAW_TYPE);
		
		if(value != null) {
			
			reference.setType(value);
			reference.setCaseLawReference(true);
			return true;
		}
	
		value = features.get(Entity.LEGAL_TYPE);
		
		if(value != null) {
			
			reference.setType(value);
			return true;
		}

		return false;
	}

	private static boolean readAuthority(LegalReference reference, Map<Entity,String> features) {
		
		if(reference.getAuthority() != null && reference.getAuthority().length() > 0) {
			return false;
		}

		String value = features.get(Entity.LEGISLATION_AUTHORITY);
		
		if(value != null) {
			
			reference.setAuthority(value);
			reference.setLegislationReference(true);
			return true;
		}
		
		value = features.get(Entity.CASELAW_AUTHORITY);
		
		if(value != null) {
			
			reference.setAuthority(value);
			reference.setCaseLawReference(true);
			return true;
		}

		/*
		 * TODO  do we need this? 
		 *
		value = features.get(CommonEntities.LEGAL_AUTHORITY);
		
		if(value != null) {
		
			reference.setAuthority(value);
		}
		*/
		
		return false;
	}
	
	private static boolean readSubject(LegalReference reference, Map<Entity,String> features) {
		
		if(reference.getSubject() != null && reference.getSubject().length() > 0) {
			return false;
		}

		String value = features.get(Entity.SUBJECT);
		
		if(value != null) {
			
			reference.setSubject(value);
			return true;
		}
	
		return false;
	}
	
	private static boolean readSection(LegalReference reference, Map<Entity,String> features) {
		
		if(reference.getSection() != null && reference.getSection().length() > 0) {
			return false;
		}

		String value = features.get(Entity.SECTION);
		
		if(value != null) {
			
			reference.setSection(value);
			return true;
		}
	
		return false;
	}
	
	private static boolean readDate(LegalReference reference, Map<Entity,String> features) {
		
		if(reference.getDate() != null && reference.getDate().length() > 0) {
			return false;
		}

		String value = features.get(Entity.DATE);
		
		if(value != null) {
			
			reference.setDate(value);
			return true;
		}
	
		return false;
	}

	private static boolean readNumber(LegalReference reference, Map<Entity,String> features) {
		
		if(reference.getNumber() != null && reference.getNumber().length() > 0) {
			return false;
		}

		String value = features.get(Entity.NUMBER);
		
		if(value != null) {
			
			reference.setFullNumber(value);
			
			String prefix = "";
			String year = "";
			
			int dash = value.indexOf("-");
			
			if(dash > -1 && (dash+1) < value.length()) {
				
				prefix = value.substring(0, dash);
				value = value.substring(dash+1);
			}
			
			int slash = value.indexOf("/");
			
			if(slash > -1 && (slash+1) < value.length()) {
				
				year = value.substring(slash+1);
				value = value.substring(0,slash);
			}
			
			reference.setYear(year);
			reference.setNumberPrefix(prefix);
			reference.setNumber(value);
			
			return true;
		}
	
		return false;
	}
	
	private static boolean readPartition(LegalReference reference, Map<Entity,String> features) {
		
		if(reference.getPartition() != null && reference.getPartition().length() > 0) {
			return false;
		}

		String value = features.get(Entity.PARTITION);
		
		if(value != null) {
			
			reference.setPartition(value);
			return true;
		}
	
		return false;
	}
	
	private static boolean readCaseNumber(LegalReference reference, Map<Entity,String> features) {
		
		if(reference.getCaseNumber() != null && reference.getCaseNumber().length() > 0) {
			return false;
		}

		String value = features.get(Entity.CASE_NUMBER);
		
		if(value != null) {
			
			reference.setFullCaseNumber(value);
			
			//ignore possible joint cases
			int semicolon = value.indexOf(";");
			
			if(semicolon > -1) {

				value = value.substring(0,semicolon);
			}
			
			String prefix = "";
			String year = "";
			
			int dash = value.indexOf("-");
			
			if(dash > -1 && (dash+1) < value.length()) {
				
				prefix = value.substring(0, dash);
				value = value.substring(dash+1);
			}
			
			int slash = value.indexOf("/");
			
			if(slash > -1 && (slash+1) < value.length()) {
				
				year = value.substring(slash+1);
				value = value.substring(0,slash);
			}
			
			reference.setCaseYear(year);
			reference.setCasePrefix(prefix);
			reference.setCaseNumber(value);
			
			return true;
		}
	
		return false;
	}

	private static boolean readAltNumber(LegalReference reference, Map<Entity,String> features) {
		
		if(reference.getAltNumber() != null && reference.getAltNumber().length() > 0) {
			return false;
		}

		String value = features.get(Entity.ALT_NUMBER);
		
		if(value != null) {
			
			reference.setFullAltNumber(value);
			
			String prefix = "";
			String year = "";
			
			int dash = value.lastIndexOf("-");
			
			if(dash > -1 && (dash+1) < value.length()) {
				
				prefix = value.substring(0, dash);
				value = value.substring(dash+1);
			}
			
			int slash = value.indexOf("/");
			
			if(slash > -1 && (slash+1) < value.length()) {
				
				year = value.substring(slash+1);
				value = value.substring(0,slash);
			}
			
			reference.setAltYear(year);
			reference.setAltPrefix(prefix);
			reference.setAltNumber(value);			
			
			return true;
		}
	
		return false;
	}
	
	private static boolean readApplicant(LegalReference reference, Map<Entity,String> features) {
		
		if(reference.getApplicant() != null && reference.getApplicant().length() > 0) {
			return false;
		}

		String value = features.get(Entity.APPLICANT);
		
		if(value != null) {
			
			reference.setApplicant(value);
			return true;
		}
	
		return false;
	}
	
	private static boolean readDefendant(LegalReference reference, Map<Entity,String> features) {
		
		if(reference.getDefendant() != null && reference.getDefendant().length() > 0) {
			return false;
		}

		String value = features.get(Entity.DEFENDANT);
		
		if(value != null) {
			
			reference.setDefendant(value);
			return true;
		}
	
		return false;
	}
	
	private static boolean readGeo(LegalReference reference, Map<Entity,String> features) {
		
		if(reference.getGeographic() != null && reference.getGeographic().length() > 0) {
			return false;
		}

		String value = features.get(Entity.GEO);
		
		if(value != null) {
			
			reference.setGeographic(value);
			return true;
		}
	
		return false;
	}
	
	private static boolean readMisc(LegalReference reference, Map<Entity,String> features) {
		
		if(reference.getMisc() != null && reference.getMisc().length() > 0) {
			return false;
		}

		String value = features.get(Entity.MISC);
		
		if(value != null) {
			
			reference.setMisc(value);
			return true;
		}
	
		return false;
	}	
}
