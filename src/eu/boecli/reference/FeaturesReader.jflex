/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.reference;

import java.util.HashMap;
import java.util.Map;

import eu.boecli.common.Entity;
import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;

%%
%class FeaturesReader 
%standalone
%unicode
%caseless
%char
%line
%column
%public

%include ../service/impl/StdMacros.in
%include ../service/impl/BoecliMacros.in

%{
	
	/* Custom java code */

	public Language getLanguage() { return Language.DEFAULT; }

	public Jurisdiction getJurisdiction() { return Jurisdiction.DEFAULT; }

	public String getName() { return "Generic reference recognition"; }
	
	public String getVersion() { return "1.1"; }
	
	public String getAuthor() { return "ITTIG"; }


	/* An empty default constructor is required to comply with BOECLIService */
	
	public FeaturesReader() { }
	
	
	public Map<Entity,String> features = new HashMap<Entity,String>();
	
%} 


/* Reads the common entities within the BOECLI annotations and put them in Key->Value map of features. */ 



%x be identifier legislationA caseLawA legalT legislationT caseLawT subjectS sectionS dateS numberS caseNumberS altNumberS applicantS defendantS defendantValueS geoS partition legislationAlias caseLawAlias miscS     

%%

{BOPEN}		{ yybegin(be); }

<be> {

	(IDENTIFIER:ECLI)[\]]	{ yybegin(identifier); }

	(LEGISLATION_ALIAS:)	{ yybegin(legislationAlias); }
	(CASELAW_ALIAS:)		{ yybegin(caseLawAlias); }


	(LEGISLATION_AUTHORITY:)	{ yybegin(legislationA); }
	(CASELAW_AUTHORITY:)		{ yybegin(caseLawA); }


	(LEGAL_TYPE:)		{ yybegin(legalT); }
	(LEGISLATION_TYPE:)	{ yybegin(legislationT); }
	(CASELAW_TYPE:)		{ yybegin(caseLawT); }


	(SUBJECT:)		{ yybegin(subjectS); }
	
	(SECTION:)		{ yybegin(sectionS); }

	
	(DATE:)			{ yybegin(dateS); }
	(NUMBER:)		{ yybegin(numberS); }
	
	(CASE_NUMBER:)		{ yybegin(caseNumberS); }
	
	(ALT_NUMBER:)		{ yybegin(altNumberS); }	
	
	(APPLICANT)[\]]		{ yybegin(applicantS); }
	(DEFENDANT)[\]]		{ yybegin(defendantS); }
	(DEFENDANT:)		{ yybegin(defendantValueS); }
	
	(GEO)				{ yybegin(geoS); }
	
	(MISC)[\]]			{ yybegin(miscS); }
	
	(PARTITION:)		{ yybegin(partition); }
	

	(\])	{ yybegin(YYINITIAL); }
	
	[^]		{ }
		
}


<identifier> {
	
	([^\[]|{E})+	{ 
						String text = yytext();
						
						features.put(Entity.IDENTIFIER, text); 
					}
					
	.				{ yybegin(YYINITIAL); }
}

<legislationAlias> {
	
	([^\]:]|{E})+	{ features.put(Entity.LEGISLATION_ALIAS, yytext()); }
	
	.				{ yybegin(YYINITIAL); }
}

<caseLawAlias> {
	
	([^\]:]|{E})+	{ features.put(Entity.CASELAW_ALIAS, yytext()); }
	
	.				{ yybegin(YYINITIAL); }
}


<legislationA> {
	
	([^\]:]|{E})+	{ features.put(Entity.LEGISLATION_AUTHORITY, yytext()); }
	
	.				{ yybegin(YYINITIAL); }
}

<caseLawA> {
	
	([^\]:]|{E})+	{ features.put(Entity.CASELAW_AUTHORITY, yytext()); }
	
	.				{ yybegin(YYINITIAL); }
}


<legalT> {
	
	([^\]:]|{E})+	{ features.put(Entity.LEGAL_TYPE, yytext()); }
	
	.				{ yybegin(YYINITIAL); }
}

<legislationT> {
	
	([^\]:]|{E})+	{ features.put(Entity.LEGISLATION_TYPE, yytext()); }
	
	.				{ yybegin(YYINITIAL); }
}

<caseLawT> {
	
	([^\]:]|{E})+	{ features.put(Entity.CASELAW_TYPE, yytext()); }
	
	.				{ yybegin(YYINITIAL); }
}



<subjectS> {
	
	([^\]:]|{E})+	{ features.put(Entity.SUBJECT, yytext()); }
	
	.				{ yybegin(YYINITIAL); }
}

<sectionS> {
	
	([^\]:]|{E})+	{ features.put(Entity.SECTION, yytext()); }
	
	.				{ yybegin(YYINITIAL); }
}


<dateS> {
	
	([^\]:]|{E})+	{ features.put(Entity.DATE, yytext()); }
	
	.				{ yybegin(YYINITIAL); }
}


<numberS> {
	
	([^\]:]|{E})+	{ features.put(Entity.NUMBER, yytext()); }
	
	.				{ yybegin(YYINITIAL); }
}



<caseNumberS> {
	
	([^\]]|{E})+	{ features.put(Entity.CASE_NUMBER, yytext()); }
	
	.				{ yybegin(YYINITIAL); }
}


<altNumberS> {
	
	([^\]:]|{E})+	{ features.put(Entity.ALT_NUMBER, yytext()); }
	
	.				{ yybegin(YYINITIAL); }
}


<applicantS> {
	
	([^\[]|{E})+	{ features.put(Entity.APPLICANT, yytext()); }
	
	.				{ yybegin(YYINITIAL); }
}

<defendantS> {
	
	([^\[]|{E})+	{ features.put(Entity.DEFENDANT, yytext()); }
	
	.				{ yybegin(YYINITIAL); }
}

<defendantValueS> {
	
	([^\]]|{E})+	{ features.put(Entity.DEFENDANT, yytext()); }
	
	.				{ yybegin(YYINITIAL); }
}

<geoS> {
	
	[\]]([^\[]|{E})+	{ features.put(Entity.GEO, yytext().substring(1)); yybegin(YYINITIAL); }
	
	(:)([^\]]|{E})+		{ features.put(Entity.GEO, yytext().substring(1)); yybegin(YYINITIAL); }
	
	.					{ yybegin(YYINITIAL); }
}

<miscS> {
	
	([^\[]|{E})+	{ 
						String text = yytext();
						
						if(features.get(Entity.MISC) != null) {
							
							text = features.get(Entity.MISC) + "; " + text;
						}
	
						features.put(Entity.MISC, text); 
					}
	
	.				{ yybegin(YYINITIAL); }
}


<partition> {
	
	([^\]]|{E})+	{ 
						
						if(features.get(Entity.PARTITION) != null) {
						
							features.put(Entity.PARTITION, features.get(Entity.PARTITION) + yytext());
							
						} else {
							
							features.put(Entity.PARTITION, yytext()); }
						}
	
	.				{ yybegin(YYINITIAL); }
}




[^]    				{  }


