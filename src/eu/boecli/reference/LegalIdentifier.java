/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.reference;

import eu.boecli.common.CommonIdentifiers;
import eu.boecli.common.Identifier;

public final class LegalIdentifier implements Comparable<LegalIdentifier> {

	
	//Kind of identifier
	private Identifier identifier = null;
	
	//Identifying string code
	private String code = "";
	
	//Optional URL for hyperlinking
	private String url = "";
	
	//Info about the module/service that assigned this LegalIdentifier
	private String provenance = "";
	
	//Reliability of the generated identifier (values between 0 and 1)
	private double confidence = 0.0D;
	

	
	//Prevent instantiations without Factory
	LegalIdentifier() { }

	
	
	public Identifier getIdentifier() {
		return identifier;
	}

	void setIdentifier(Identifier identifier) {
		this.identifier = identifier;
	}

	public String getCode() {
		return code;
	}

	void setCode(String code) {
		this.code = code;
	}
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getProvenance() {
		return provenance;
	}

	public void setProvenance(String provenance) {
		this.provenance = provenance;
	}
	
	public String getConfidenceValue() {
		
		double conf = getConfidence();
		
		double roundOff = (double) Math.round(conf * 100) / 100;
		
		return String.valueOf(roundOff);
	}
	
	private double getConfidence() {
		return confidence;
	}

	public boolean setConfidence(double confidence) {
		
		if(confidence < 0 || confidence > 1) {
			
			return false;
		}
		
		this.confidence = confidence;
		return true;
	}

	@Override
	public int compareTo(LegalIdentifier o) {
		
		if(this.getConfidence() > o.getConfidence()) return -1;
		if(this.getConfidence() < o.getConfidence()) return 1;
		
		if(this.getIdentifier().equals(CommonIdentifiers.ECLI) && !o.getIdentifier().equals(CommonIdentifiers.ECLI)) return -1;
		if( !this.getIdentifier().equals(CommonIdentifiers.ECLI) && o.getIdentifier().equals(CommonIdentifiers.ECLI)) return 1;
		
		if(this.getIdentifier().equals(CommonIdentifiers.ELI) && !o.getIdentifier().equals(CommonIdentifiers.ELI)) return -1;
		if( !this.getIdentifier().equals(CommonIdentifiers.ELI) && o.getIdentifier().equals(CommonIdentifiers.ELI)) return 1;
		
		if(this.getIdentifier().equals(CommonIdentifiers.CELEX) && !o.getIdentifier().equals(CommonIdentifiers.CELEX)) return -1;
		if( !this.getIdentifier().equals(CommonIdentifiers.CELEX) && o.getIdentifier().equals(CommonIdentifiers.CELEX)) return 1;
		
		return this.getCode().compareTo(o.getCode());
	}
	
}
