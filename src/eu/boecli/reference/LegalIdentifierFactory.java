/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.reference;

import eu.boecli.common.CommonIdentifiers;
import eu.boecli.common.Identifier;

public final class LegalIdentifierFactory {

	
	public static LegalIdentifier createLegalIdentifier(Identifier identifier, String code) {
		
		if(identifier == null || code == null || code.trim().length() < 3) {
			
			return null;
		}
		
		
		LegalIdentifier legalIdentifier = new LegalIdentifier();
		
		legalIdentifier.setIdentifier(identifier);
		
		legalIdentifier.setCode(code);
		
		
		//Set up default URLs for common identifiers
		
		if(identifier.equals(CommonIdentifiers.ECLI)) {
			
			legalIdentifier.setUrl("https://e-justice.europa.eu/ecli/" + code);
		}
		
		if(identifier.equals(CommonIdentifiers.ELI)) {
			
			legalIdentifier.setUrl(code);
		}
		
		if(identifier.equals(CommonIdentifiers.CELEX)) {
			
			legalIdentifier.setUrl("http://eur-lex.europa.eu/legal-content/TXT/?uri=" + code);
		}
		
		return legalIdentifier;
	}
}
