/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.service;

import eu.boecli.engine.EngineDocument;
import eu.boecli.service.impl.Util;
import eu.boecli.service.rendering.RenderingService;

public abstract class AnnotationService extends BOECLIService {
	
	/*
	 * 
	 * An annotation service can add a temporary mark-up on the underlying text.
	 * Both entity identification and reference recognition are annotation services.
	 * This class provides common code for both.
	 * 
	 * It mantains track of the text before and after execution.
	 */
	
	protected String before = "";
	
	protected StringBuilder after = null;
	

	public String getBefore() {
		return before;
	}

	public String getAfter() {
		return after.toString();
	}

	public void initAnnotations(String before) {
		
		this.before = before;
		
		this.after = new StringBuilder();
	}



	/*
	 * Style of the internal temporary textual annotations.
	 * [BOECLI:...:...]text text text[/BOECLI]
	 */
	protected final static String O = "["; //open
	
	protected final static String C = "]"; //close
	
	protected final static String OC = "[/"; //open/close
	
	protected final static String S = ":"; //separator
	
	protected final static String BE = "BOECLI"; //Boecli prefix
	
	protected final static String OBE = O + BE + S; //open boecli annotation
	
	protected final static String CBE = OC + BE + C; //close boecli annotation
	
	
	
	/*
	 * Common auxiliary method for sending text to the output as it is (without annotations).
	 */
	protected final void addText(String text) {
	
		//TODO syntax check on 'text'
		
		after.append(text);
	}

	
	/*
	 * Checks input and output text for errors.
	 */
	@Override
	protected final boolean checkBeforeRun() {

		//TODO check the text before and after the run of the service: it must match (besides annotations)

		//TODO check after the run the syntax of the annotations
		//ex.: [BOECLI:CASELAW_AUTHORITY:IT_[BOECLI:TYPE:CONSTITUTION]COST[/BOECLI]]
		
		//TODO check after the run that there are no nested annotations
		
		//TODO check after the run that there are no unbalanced annotations

		return true;
	}
	

	/*
	 * Checks input and output text for errors.
	 */
	@Override
	protected final boolean checkAfterRun() {

		if(this instanceof RenderingService) {
			return true;
		}
		
		String cleanBefore = Util.removeAllAnnotations(before);
		String cleanAfter = Util.removeAllAnnotations(after.toString());
		
		//Check the text before and after the run of the service: it must match (besides annotations)
		if( !cleanBefore.equals(cleanAfter)) {
			
			((EngineDocument) getEngineDocument()).addError("Annotation service error: before and after text don't match!");
			((EngineDocument) getEngineDocument()).addError("BEFORE:" + before);
			((EngineDocument) getEngineDocument()).addError("AFTER: " + after.toString());
			((EngineDocument) getEngineDocument()).addError("CLEANBEFORE:" + cleanBefore);
			((EngineDocument) getEngineDocument()).addError("CLEANAFTER: " + cleanAfter);
			((EngineDocument) getEngineDocument()).addError("EQUALPART: " + Util.getEqualPart(cleanBefore, cleanAfter));
			
			return false;
		}


		//TODO check after the run the syntax of the annotations
		//ex.: [BOECLI:CASELAW_AUTHORITY:IT_[BOECLI:TYPE:CONSTITUTION]COST[/BOECLI]]
		
		//TODO check after the run that there are no nested annotations
		
		//TODO check after the run that there are no unbalanced annotations
		
		return true;
	}

}
