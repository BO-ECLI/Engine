/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.service;

import eu.boecli.common.Language;
import eu.boecli.engine.BOECLIEngine;
import eu.boecli.engine.BOECLIEngineDocument;
import eu.boecli.engine.EngineDocument;
import eu.boecli.service.generation.IdentifierGenerationService;
import eu.boecli.service.identification.EntityIdentificationService;
import eu.boecli.service.recognition.ReferenceRecognitionService;

public abstract class BOECLIService implements Comparable<BOECLIService>, ServiceInfo {
	
	
	/*
	 * 
	 * Generic BOECLI service.
	 * 
	 * Specializations consist in:
	 * - entity identification services
	 * - reference recognition services
	 * - identifier generation services
	 *
	 * Provides methods for accessing the 
	 * underlying BOECLIEngineDocument that
	 * contains:
	 * - metadata of the input
	 * - the annotated text
	 * - the identified legal references 
	 * 
	 */
	
	private int index = -1; //Relative position in a META-INF/services file
	
	public final void setIndex(int index) {
		
		if(this.index == -1) {
		
			this.index = index;
		}
	}
	
	public final int getIndex() {
		
		return this.index;
	}
	
	
	private BOECLIEngineDocument engineDocument = null;
	
	protected final BOECLIEngineDocument getEngineDocument() {
		
		return engineDocument;
	}
	
	
	/*
	 * Init service.
	 */
	public final boolean init(BOECLIEngineDocument engineDocument) {
		
		this.engineDocument = engineDocument;
		
		if(this instanceof AnnotationService) {
			
			((AnnotationService) this).initAnnotations(getEngineDocument().getAnnotatedText());
		}

		return true;
	}
	
	/*
	 * Short description of the specific implementation.
	 */
	public final String getDescription() {
		
		String description = this.getClass().getName() + " [" + serviceName() + " (v. " + version() + ")] by " + author() + " for language " + language() +
								" and jurisdiction " + jurisdiction() + " in extension " + extensionName();
		
		return description;
	}

	
	/*
	 * Execute the BOECLI Service.
	 */
	public final boolean runService() {

		if( !checkBeforeRun()) {
			return false;
		}
		
		if(BOECLIEngine.DEBUG) System.out.println("Running " + this.getDescription());
		((EngineDocument) getEngineDocument()).addMessage("Running " + this.getDescription());
		
		if( !run()) {
			return false;
		}
		
		return checkAfterRun();
	}
	
	/*
	 * Specific implementation of the service.
	 */
	protected abstract boolean run();

	
	/*
	 * Run this service before or after the execution of the defaults services.
	 */
	/*
	public boolean runBeforeDefault() {
		
		return true;
	}
	*/
	
	
	public boolean highPriority() {
		
		return false;
	}
	
	public boolean lowPriority() {
		
		return false;
	}
	
	
	
	/*
	 * Enable/disable the execution of the service.
	 * A service could be disabled if the engineDocument (the input)
	 * doesn't meet specific requirements of the service.
	 * For example, a service for the recognition of patterns of a specific court
	 * should be disabled if the issuing authority of the input is a different court. 
	 */
	public boolean runnable() {
		
		return true;
	}
	
	/*
	 * 
	 */
	protected boolean checkBeforeRun() {

		return true;
	}

	/*
	 * 
	 */
	protected boolean checkAfterRun() {

		return true;
	}
	
	@Override
	public final int compareTo(BOECLIService o) {

		/*
		 * preprocessing services
		 * entity identification
		 * reference recognition
		 * identifier generation
		 * rendering services
		 * 
		 * high
		 * low 
		 * 
		 * default
		 * national
		 *  
		 * order of appearance in services file
		 * (using a counter)
		 */
		
		if( (this instanceof EntityIdentificationService) && !(o instanceof EntityIdentificationService) ) return -1;
		if( !(this instanceof EntityIdentificationService) && (o instanceof EntityIdentificationService) ) return 1;
		
		if( (this instanceof ReferenceRecognitionService) && !(o instanceof ReferenceRecognitionService) ) return -1;
		if( !(this instanceof ReferenceRecognitionService) && (o instanceof ReferenceRecognitionService) ) return 1;
		
		if( (this instanceof IdentifierGenerationService) && !(o instanceof IdentifierGenerationService) ) return -1;
		if( !(this instanceof IdentifierGenerationService) && (o instanceof IdentifierGenerationService) ) return 1;
		
		//if( (this instanceof RenderingService) && !(o instanceof RenderingService) ) return -1;
		//if( (this instanceof RenderingService) && !(o instanceof RenderingService) ) return 1;
		
		if(this.highPriority() && !o.highPriority()) return -1;
		if( !this.highPriority() && o.highPriority()) return 1;
		
		if(this.lowPriority() && !o.lowPriority()) return 1;
		if( !this.lowPriority() && o.lowPriority()) return -1;
		
		if(this.language().equals(Language.DEFAULT) && !o.language().equals(Language.DEFAULT)) return -1;
		if( !this.language().equals(Language.DEFAULT) && o.language().equals(Language.DEFAULT)) return 1;
		
		if(this.getIndex() < o.getIndex()) return -1;
		if(this.getIndex() > o.getIndex()) return 1;
		
		return this.getDescription().compareTo(o.getDescription());
	}

}
