/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.service.generation;

import java.util.Collection;

import eu.boecli.reference.LegalIdentifier;
import eu.boecli.reference.LegalReference;
import eu.boecli.service.BOECLIService;

public abstract class IdentifierGenerationService extends BOECLIService {

	/*
	 * 
	 * A generic identifier generation service for legal references.
	 * 
	 */

	
	@Override
	protected final boolean run() {

		for(LegalReference legalReference : getEngineDocument().getLegalReferences()) {
			
			Collection<LegalIdentifier> legalIdentifiers = null;
			
			try {
					
				legalIdentifiers = getLegalIdentifiers(legalReference);
					
			} catch (IllegalArgumentException e) {
					
				//Ignore the value
				legalIdentifiers = null;
			}
					
			if(legalIdentifiers != null) {

				for(LegalIdentifier legalIdentifier : legalIdentifiers) {
				
					//TODO check the type of Identifier
					//TODO syntax check of the code?
	
					legalIdentifier.setProvenance(getDescription());
					legalReference.addIdentifier(legalIdentifier);
				}
			}
		}

		return true;
	}	
	
	protected abstract Collection<LegalIdentifier> getLegalIdentifiers(LegalReference legalReference) throws IllegalArgumentException;

}
