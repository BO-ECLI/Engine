/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.service.recognition;

import java.io.IOException;
import java.io.StringReader;
import java.util.Map;

import eu.boecli.common.CommonCaseLawAuthorities;
import eu.boecli.common.Entity;
import eu.boecli.engine.EngineDocument;
import eu.boecli.reference.FeaturesReader;
import eu.boecli.reference.LegalReference;
import eu.boecli.reference.LegalReferenceFactory;
import eu.boecli.service.AnnotationService;
import eu.boecli.service.impl.Util;

public abstract class ReferenceRecognitionService extends AnnotationService {
	
	
	/*
	 * 
	 * A generic reference recognition service.
	 * Provides auxiliary methods for adding a legal reference.
	 * 
	 * Instantiates the legal reference objects.
	 * 
	 */
	
	/*
	 * Auxiliary methods
	 */

	protected final void addReference(String text) {
		
		addReference(text, "", null);
	}

	protected final void addReference(String text, String context) {
		
		addReference(text, context, null);
	}

	protected final void addReference(String text, Map<Entity,String> customFeatures) {
		
		addReference(text, "", customFeatures);
	}

	/*
	 * text, context and customFeatures
	 */
	protected final void addReference(String text, String context, Map<Entity,String> customFeatures) {	
		
		Map<Entity,String> textFeatures = readTextualFeatures(text);
		Map<Entity,String> contextFeatures = readTextualFeatures(context);
		
		LegalReference reference = LegalReferenceFactory.createLegalReference(textFeatures, contextFeatures, customFeatures, getEngineDocument());
		
		if(reference == null) {

			((EngineDocument) getEngineDocument()).addMessage("addReference() - reference is null. text: " + text);
			
			return;			
		}
		
		reference.setProvenance(getDescription());
		reference.setText(Util.removeAllAnnotations(text));
		
		if(context != null && context.trim().length() > 0) {
			
			reference.setContext(Util.removeAllAnnotations(context));
		}

		//Add custom features
		addCustomFeatures(reference);
		
		//Add common features
		addCommonFeatures(reference);
		
		addText(OBE + Entity.LEGAL_REFERENCE + S + reference.getId() + C + reference.getText() + CBE);
	}
	
	/*
	 * Read the annotations and translate them into a map Entity->Value.
	 * 
	 * Implemented with JFlex
	 */
	private Map<Entity,String> readTextualFeatures(String text) {
	
		if(text == null || text.trim().length() < 1) {
			return null;
		}
		
		FeaturesReader rfr = new FeaturesReader();
		
		try {
			
			rfr.yyreset(new StringReader(text));
			
			rfr.yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
		}
		
		return rfr.features;
	}
	
	private LegalReference readLegalReference(String text) {
		
		String prefix = "BOECLI:LEGAL_REFERENCE:";
		String suffix = "]";
		
		int start = text.indexOf(prefix);
		
		if(start < 0) {
			return null;
		}
		
		int end = text.indexOf(suffix, start);
		
		if(end < 0) {
			return null;
		}

		String id = text.substring(start + prefix.length(), end);
		
		return ((EngineDocument) getEngineDocument()).getLegalReference(id);
	}
	
	private String readPartition(String text) {
		
		String prefix = "BOECLI:PARTITION:";
		String suffix = "]";
		
		int start = text.indexOf(prefix);
		
		if(start < 0) {
			return "";
		}
		
		int end = text.indexOf(suffix, start);
		
		if(end < 0) {
			return "";
		}

		return text.substring(start + prefix.length(), end);
	}
	
	//text: "[BOECLI:PARTITION:article-24;]24[/BOECLI] [BOECLI:LEGAL_REFERENCE:1]Cost.[/BOECLI]"
	protected final void extendReference(String text, String context) {
		
		//Get the legal reference from the legal reference annotation
		LegalReference legalReference = readLegalReference(text);
		
		//Update the legal reference text and context
		legalReference.setText(Util.removeAllAnnotations(text));
		legalReference.setContext(Util.removeAllAnnotations(context));

		//Read the partition value
		String partition = readPartition(text);
		
		if(legalReference == null || partition.equals("")) {
			
			addText(text);
			return;
		}

		//Add a partition value to the legal reference
		legalReference.setPartition(partition);
		
		//Annotate the whole context  
		addText(OBE + Entity.LEGAL_REFERENCE + S + legalReference.getId() + C + legalReference.getText() + CBE);
	}
	
	//context: "[BOECLI:PARTITION:article-3;]artt. 3[/BOECLI] [BOECLI:STOPWORD:AND]e[/BOECLI] [BOECLI:PARTITION:article-24;]24[/BOECLI] [BOECLI:LEGAL_REFERENCE:1]Cost.[/BOECLI]"
	//text: "[BOECLI:PARTITION:article-3;]artt. 3[/BOECLI]"
	protected final void cloneReference(String text, String context) {
		
		//Get the legal reference from the legal reference annotation
		LegalReference legalReference = readLegalReference(context);
		
		//Read the partition value
		String partition = readPartition(text);
		
		if(legalReference == null || partition.equals("")) {
			
			addText(text);
			return;
		}
		
		//Update the legal reference context
		legalReference.setContext(Util.removeAllAnnotations(context));
		
		//Instantiate a new legal reference object and add it to the collection
		LegalReference newLegalReference = LegalReferenceFactory.cloneLegalReference(legalReference, getEngineDocument());
		
		//Set the partition value to the new legal reference
		newLegalReference.setPartition(partition);
		
		text = Util.removeAllAnnotations(text);
		
		//Update the new legal reference text
		newLegalReference.setText(text);
		
		//Annotate only text with the new legal reference		
		addText(OBE + Entity.LEGAL_REFERENCE + S + newLegalReference.getId() + C + newLegalReference.getText() + CBE);
	}
	
	protected void addCustomFeatures(LegalReference legalReference) {
		
	}
	
	protected void addCommonFeatures(LegalReference legalReference) {
		
		//If auth not specified, and defendant is a country, then auth=ECHR
		if(legalReference.getAuthority().equals("")) {
			
			if( !legalReference.getApplicant().equals("") && legalReference.getDefendant().length() == 2) {
				
				//TODO check the defendant value with an authoritative list of possible country codes
				legalReference.setAuthority(CommonCaseLawAuthorities.ECHR.toString());
			}
		}
		
		//If auth not specified, and the case number starts with "C-", then auth=CJEU
		if(legalReference.getAuthority().equals("")) {
			
			if(legalReference.getCaseNumber(true).startsWith("C-")) {
				
				//TODO check also the raw text?
				legalReference.setAuthority(CommonCaseLawAuthorities.CJEU.toString());
			}
		}
	}
}

