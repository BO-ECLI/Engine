/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.service.impl.rendering;

import java.io.IOException;
import java.io.StringReader;

import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.engine.BOECLIEngine;
import eu.boecli.engine.EngineDocument;
import eu.boecli.reference.LegalIdentifier;
import eu.boecli.reference.LegalReference;
import eu.boecli.service.rendering.RenderingService;

%%
%class DefaultHtmlRendering 
%extends RenderingService
%standalone
%unicode
%caseless
%char
%line
%column
%public

%include ../StdMacros.in
%include ../BoecliMacros.in

%{
	
	/* Custom java code */

	@Override
	public Language language() { return Language.DEFAULT; }
	
	@Override
	public Jurisdiction jurisdiction() { return Jurisdiction.DEFAULT; }

	@Override
	public String serviceName() { return "Default HTML Rendering"; }
	
	@Override
	public String extensionName() { return "Default Extension"; }

	@Override
	public String version() { return "0.3"; }
	
	@Override
	public String author() { return "ITTIG"; }

	/* An empty default constructor is required to comply with BOECLIService */
	public DefaultHtmlRendering() { }
	
	@Override
	public final boolean run() {
		
		try {
			
			yyreset(new StringReader(getEngineDocument().getAnnotatedText()));
			yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	private boolean idSaved = false;
	
	private boolean isFont = false;
	
	private void getLegalReference(String id) {
	
		if(idSaved) {
			return;
		}
		
		LegalReference reference = ((EngineDocument) getEngineDocument()).getLegalReference(id);
		
		if(reference == null) {
			
			((EngineDocument) getEngineDocument()).addMessage("DefaultHtmlRendering error. reference is null for id: " + id + " (" + yytext() + ")");
		
		} else {
			
			addHref(reference);
		}
	}
	
	private void addHref(LegalReference reference) {
		
		String href = "";
		
		if(reference != null) {
			
			LegalIdentifier legalIdentifier = null;
			
			for(LegalIdentifier item : reference.getLegalIdentifiers()) {

				//Choose the first identifier that provides a URL
				if(item.getUrl().length() > 1) {
					legalIdentifier = item;
					break;
				}
			}
			
			if(legalIdentifier != null) {
				
				href += "<a href=\"" + legalIdentifier.getUrl();
			}
		}
		
		if( !href.equals("")) {
			
			addText(href + "\" target=\"_new\">");
			isFont = false;
			
		} else {
		
			addText("<font color=\"red\">");
			isFont = true;
		}
	}

%} 

/* Remove all the BOECLI annotations from the text. */


%x be beLegal 

%%


{BOPEN}(LEGAL_REFERENCE:)		{ yybegin(beLegal); }


{BCLOSE}	{ }



<beLegal> {

	([0-9]+(:))/([0-9]+[\]])		{ 
										getLegalReference(yytext().substring(0,yylength()-1)); 
										idSaved = true; 
									}
	
	[0-9]+[\]]		{  
						getLegalReference(yytext().substring(0,yylength()-1)); 
						idSaved = true; 
					}
		
	{BCLOSE}		{ 
						if(isFont) {
							addText("</font>");
						} else {
							addText("</a>"); 
						}
						
						idSaved = false; 
						yybegin(YYINITIAL); 
					}
	
	[^]				{ 
						addText(yytext()); 
					}
}



<be> {

	(\])	{ yybegin(YYINITIAL); }
	[^]		{ }
}

{DD}		{
				if(BOECLIEngine.DEBUG) {
					addText("<strong>" + yytext() + "</strong>");
				} else {
					addText(yytext());
				}
			}

{E}			{ addText("</br>"); }
[^]			{ addText(yytext()); }


