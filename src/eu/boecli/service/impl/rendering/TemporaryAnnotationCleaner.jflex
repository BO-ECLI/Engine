/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.service.impl.rendering;

import java.io.IOException;
import java.io.StringReader;

import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.service.rendering.RenderingService;


%%
%class TemporaryAnnotationCleaner 
%extends RenderingService
%standalone
%unicode
%caseless
%char
%line
%column
%public

%include ../StdMacros.in
%include ../BoecliMacros.in

%{
	
	/* Custom java code */
	
	@Override
	public Language language() { return Language.DEFAULT; }
	
	@Override
	public Jurisdiction jurisdiction() { return Jurisdiction.DEFAULT; }

	@Override
	public String serviceName() { return "Temporary annotation cleaner service"; }
	
	@Override
	public String extensionName() { return "Default Extension"; }

	@Override
	public String version() { return "0.1"; }
	
	@Override
	public String author() { return "ITTIG"; }
  	

	/* An empty default constructor is required to comply with BOECLIService */
	public TemporaryAnnotationCleaner() { }
	
	@Override
	public final boolean run() {
		
		try {
			
			yyreset(new StringReader(getEngineDocument().getAnnotatedText()));
			yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
%} 

/* Remove all the BOECLI annotations from the text. */


%x be beLegal 

%%

{BOPEN}		{ yybegin(be); }
{BCLOSE}	{ }


{BOPEN}(LEGAL_REFERENCE)		{ addText(yytext()); yybegin(beLegal); }


<beLegal> {

	{BCLOSE}{BCLOSE}?		{ addText(yytext()); yybegin(YYINITIAL); }
	[^]						{ addText(yytext()); }
}



<be> {

	(\])	{ yybegin(YYINITIAL); }
	[^]		{ }
}


[^]			{ addText(yytext()); }


