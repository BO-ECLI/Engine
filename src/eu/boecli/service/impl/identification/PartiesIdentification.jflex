/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.service.impl.identification;

import java.io.IOException;
import java.io.StringReader;

import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.service.identification.EntityIdentificationService;

%%
%class PartiesIdentification 
%extends EntityIdentificationService
%standalone
%unicode
%caseless
%char
%line
%column
%public

%include ../StdMacros.in
%include ../BoecliMacros.in

%{
	
	/* Custom java code */

 	@Override
	public Language language() { return Language.DEFAULT; }
  	
  	@Override
	public Jurisdiction jurisdiction() { return Jurisdiction.DEFAULT; }

  	@Override
	public String serviceName() { return "Default Parties Identification"; }
	
	@Override
	public String extensionName() { return "Default Extension"; }

  	@Override
	public String version() { return "1.4"; }
	
  	@Override
	public String author() { return "ITTIG"; }
	
	/* An empty default constructor is required to comply with BOECLIService */
	public PartiesIdentification() { }
	
	@Override
	public final boolean run() {
		
		try {
			
			yyreset(new StringReader(getEngineDocument().getAnnotatedText()));
			
			yylex();
			
			
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	@Override
	public boolean lowPriority() { return true; }
	
	
	private String partitionsText = "";
	private String partitionText = "";
	
	private int offset = 0;
	private int length = 0;

	private boolean vs = false;	

	private void saveCountry() {
	
		String text = yytext();
		
		//[BOECLI:GEO:..]
		
		String value = text.substring(12,14);
		
		addDefendant(value, text);
	}
	
%} 

/* BE_SEP = ({SCDHE}|{BE_STOP}|{BRACKETS}|[:;\|])+ */
PartySep = ({SCDHE}|{BE_STOP}|{BRACKETS}|(;))+

BE_ANN = {BE_AUTH}|{BE_TYPE}|{BE_NUMBERYEAR}|{BE_NUMBER}|{BE_CASENUMBER}|{BE_ALTNUMBER}|{BE_DATE}|{BE_SECTION}|{BE_SUBJECT}|{BE_MISC}|{BE_PART}|{BE_GEO}

Country = {BOPEN}GEO:[A-Z][A-Z]\]{BE_BODY}

PartyWordCap = \(?{CAP_WORD}\.?\)?
PartyWord = \(?{WORD}\.?\)?
GeoWord = \(?{BE_GEO}\.?\)?

PartyCountry = {PartyWordCap}({PartySep}?({PartyWord}|{GeoWord})){0,7}

PartyVs = {PartyWordCap}({PartySep}?({PartyWord}|{GeoWord})){0,5}  /* con annotazione BE in fondo */
PartyVsCap = {PartyWordCap}({PartySep}?({PartyWordCap}|{GeoWord})){0,3} /* senza annotazione in fondo */

PartyBe = {PartyWordCap}({PartySep}?({PartyWord}|{GeoWord})){0,4} /* con annotazione BE in fondo */
PartyAlone = {PartyWordCap}({PartySep}?({PartyWordCap}|{GeoWord})){0,3} /* senza annotazione in fondo */

PartyVsCountry = ({BE_ANN}{PartySep}?)?{PartyCountry}{SE}?{BE_VERSUS}{SE}?{Country}({PartySep}?{BE_ANN})?

PartyVsParty = ({BE_ANN}{PartySep}?)?{PartyVs}{SE}?{BE_VERSUS}{SE}?{PartyVs}{PartySep}?{BE_ANN}
PartyVsPartyCap = ({BE_ANN}{PartySep}?)?{PartyVs}{SE}?{BE_VERSUS}{SE}?{PartyVsCap}

/* BeParty = {BE_ANN}{PartySep}?{PartyBe}{PartySep}?{BE_ANN} */
/* BeParty = ({BE_ANN}{PartySep}?{PartyBe})/({PartySep}?{BE_ANN})*/
BePartyCap = {BE_ANN}{PartySep}?{PartyAlone}



%x BE partyVsCountryState partyVsPartyState partyVsPartyCapState bePartyState bePartyCapState        


%%



{PartyVsCountry}		{ vs = false; offset = 0; length = yylength(); yypushback(length); yybegin(partyVsCountryState); }

{PartyVsParty}			{ vs = false; offset = 0; length = yylength(); yypushback(length); yybegin(partyVsPartyState); }

{PartyVsPartyCap}		{ vs = false; offset = 0; length = yylength(); yypushback(length); yybegin(partyVsPartyCapState); }						

/* {BeParty}				{ vs = false; offset = 0; length = yylength(); yypushback(length); yybegin(bePartyState); } */
({BE_ANN}{PartySep}?{PartyBe})/({PartySep}?{BE_ANN})	{ vs = false; offset = 0; length = yylength(); yypushback(length); yybegin(bePartyState); }

{BePartyCap}			{ vs = false; offset = 0; length = yylength(); yypushback(length); yybegin(bePartyCapState); }



<partyVsCountryState> {

	{Country}		{ saveCountry(); offset += yylength(); }
	
	{PartyCountry}	{ addApplicant(yytext()); offset += yylength(); }
	
	{BE_VERSUS}		{ addText(yytext()); vs = true; offset += yylength(); }

	{BE_ANN}		{ if(offset < length) { addText(yytext()); offset += yylength(); } else { yypushback(yylength()); yybegin(YYINITIAL); } }
	
	{BE_STOP}		{ if(offset < length) { addText(yytext()); offset += yylength(); } else { yypushback(yylength()); yybegin(YYINITIAL); } }
	
	[^] {
			offset++;
			
			if(offset >= length) {
				
				if(offset > length) yypushback(1);
					
				yybegin(YYINITIAL);
			}
			
			addText(yytext());
		}	
}


<partyVsPartyState> {

	{PartyVs}/({SE}?{BE_VERSUS})	{ addApplicant(yytext()); offset += yylength(); }
	
	{PartyVs}	{ 
					if(vs) {
						addDefendant(yytext());
					} else {
						addText(yytext());
					}
					offset += yylength(); 
				}
	
	{BE_VERSUS}		{ addText(yytext()); vs = true; offset += yylength(); }
	
	{BE_ANN}		{ if(offset < length) { addText(yytext()); offset += yylength(); } else { yypushback(yylength()); yybegin(YYINITIAL); } }
	
	{BE_STOP}		{ if(offset < length) { addText(yytext()); offset += yylength(); } else { yypushback(yylength()); yybegin(YYINITIAL); } }
	
	[^] {
			offset++;
			
			if(offset >= length) {
				
				if(offset > length) yypushback(1);
					
				yybegin(YYINITIAL);
			}
			
			addText(yytext());
		}	
}


<partyVsPartyCapState> {

	{PartyVs}/({SE}?{BE_VERSUS})	{ addApplicant(yytext()); offset += yylength(); }
	
	{PartyVsCap}	{ 
						if(vs) {
							addDefendant(yytext());
						} else {
							addText(yytext());
						}
						offset += yylength(); 
					}
	
	{BE_VERSUS}		{ addText(yytext()); vs = true; offset += yylength(); }
	
	{BE_ANN}		{ if(offset < length) { addText(yytext()); offset += yylength(); } else { yypushback(yylength()); yybegin(YYINITIAL); } }
	
	{BE_STOP}		{ if(offset < length) { addText(yytext()); offset += yylength(); } else { yypushback(yylength()); yybegin(YYINITIAL); } }
	
	[^] {
			offset++;
			
			if(offset >= length) {
				
				if(offset > length) yypushback(1);
					
				yybegin(YYINITIAL);
			}
			
			addText(yytext());
		}	
}


<bePartyState> {

	{PartyBe}		{ addApplicant(yytext()); offset += yylength(); }
	
	{BE_ANN}		{ if(offset < length) { addText(yytext()); offset += yylength(); } else { yypushback(yylength()); yybegin(YYINITIAL); } }
	
	{BE_STOP}		{ if(offset < length) { addText(yytext()); offset += yylength(); } else { yypushback(yylength()); yybegin(YYINITIAL); } }
	
	[^] {
			offset++;
			
			if(offset >= length) {
				
				if(offset > length) yypushback(1);
					
				yybegin(YYINITIAL);
			}
			
			addText(yytext());
		}	
}


<bePartyCapState> {

	{PartyAlone}	{ addApplicant(yytext()); offset += yylength(); }
	
	{BE_ANN}		{ if(offset < length) { addText(yytext()); offset += yylength(); } else { yypushback(yylength()); yybegin(YYINITIAL); } }
	
	{BE_STOP}		{ if(offset < length) { addText(yytext()); offset += yylength(); } else { yypushback(yylength()); yybegin(YYINITIAL); } }
	
	[^] {
			offset++;
			
			if(offset >= length) {
				
				if(offset > length) yypushback(1);
					
				yybegin(YYINITIAL);
			}
			
			addText(yytext());
		}	
}

{BOPEN}		{ 
				addText(yytext()); 
				yybegin(BE); 
			}

<BE> {

	{BCLOS}			{ 
						yypushback(yylength()); 
						yybegin(YYINITIAL); 
					}
	
	[^]				{ 
						addText(yytext()); 
					}
}

[^]					{ addText(yytext()); }


