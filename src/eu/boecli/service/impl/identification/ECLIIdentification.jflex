/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.service.impl.identification;

import java.io.IOException;
import java.io.StringReader;

import eu.boecli.common.CommonIdentifiers;
import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.service.identification.EntityIdentificationService;

%%
%class ECLIIdentification 
%extends EntityIdentificationService
%standalone
%unicode
%caseless
%char
%line
%column
%public

%include ../StdMacros.in
%include ../BoecliMacros.in

%{
	
	/* Custom java code */

 	@Override
	public Language language() { return Language.DEFAULT; }
  	
  	@Override
	public Jurisdiction jurisdiction() { return Jurisdiction.DEFAULT; }

  	@Override
	public String serviceName() { return "ECLI identification"; }
	
	@Override
	public String extensionName() { return "Default Extension"; }

  	@Override
	public String version() { return "0.2"; }
	
  	@Override
	public String author() { return "ITTIG"; }
	
	@Override
	public boolean highPriority() { return true; }
	
	/* An empty default constructor is required to comply with BOECLIService */
	public ECLIIdentification() { }
	
	@Override
	public final boolean run() {
		
		try {
			
			yyreset(new StringReader(getEngineDocument().getAnnotatedText()));
			
			yylex();
			
			
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	


%} 

/* Identification of ECLI codes */ 


L = [A-Za-z]
D = [0-9]

NLD = [^A-Za-z0-9]

LD = {L}|{D}

LDD = {L}|{D}|(\.)


Country = {L}{L}

Authority = {LD}?{LD}?{LD}?{LD}?{LD}?{LD}?{LD}?

Ordinal = {LDD}?{LDD}?{LDD}?{LDD}?{LDD}?{LDD}?{LDD}?{LDD}?{LDD}?{LDD}?{LDD}?{LDD}?{LDD}?{LDD}?{LDD}?{LDD}?{LDD}?{LDD}?{LDD}?{LDD}?{LDD}?{LDD}?{LDD}?{LDD}?{LDD}?

Sep = {SE}?(:){SE}?

Prefix = (ecli){SE}?(:)?{SE}?

Code = {Prefix}?((ecli){Sep})?{Country}{Sep}{Authority}{Sep}{D}{D}{D}{D}{Sep}{Ordinal}



%x     

%%


{NLD}{Code}{NLD}	{
						addText(yytext().substring(0,1));
						addIdentifier(CommonIdentifiers.ECLI, yytext().substring(1,yylength()-1));
						yypushback(1);
					}


[^]    				{ addText(yytext()); }


