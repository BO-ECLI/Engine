/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.service.impl.identification;

import java.io.IOException;
import java.io.StringReader;

import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.service.identification.EntityIdentificationService;

%%
%class PartitionsIdentification 
%extends EntityIdentificationService
%standalone
%unicode
%caseless
%char
%line
%column
%public

%include ../StdMacros.in
%include ../BoecliMacros.in

%{
	
	/* Custom java code */

 	@Override
	public Language language() { return Language.DEFAULT; }
  	
  	@Override
	public Jurisdiction jurisdiction() { return Jurisdiction.DEFAULT; }

  	@Override
	public String serviceName() { return "Default Partitions Identification"; }
	
	@Override
	public String extensionName() { return "Default Extension"; }

  	@Override
	public String version() { return "1.2"; }
	
  	@Override
	public String author() { return "ITTIG"; }
	
	/* An empty default constructor is required to comply with BOECLIService */
	public PartitionsIdentification() { }
	
	@Override
	public final boolean run() {
		
		try {
			
			yyreset(new StringReader(getEngineDocument().getAnnotatedText()));
			
			yylex();
			
			
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	@Override
	public boolean lowPriority() { return true; }
	
	
	private boolean debug = false;
	
	private String partitionsText = "";
	private String partitionText = "";
	
	int offset = 0;
	int length = 0;
	
	int readOffset = 0;
	int readLength = 0;

	private boolean newArticle = false;
	private boolean newParagraph = false;
	private boolean newLetter = false;
	private boolean newItem = false;
	
	private String articleId = "";
	private String paragraphId = "";
	private String letterId = "";
	private String itemId = "";

	private String a = "";
	private String p = "";
	private String l = "";
	private String i = "";
		 

	private void saveDown() {
	
		partitionText = yytext(); 
		readOffset = 0; 
		readLength = yylength(); 
		offset += yylength(); 
		yypushback(readLength); 
		yybegin(ReadDown);
	}

	private void readPartitionIdDown() {
	
		if( !articleId.equals("")) {
		
			a = articleId;
		}
		
		if( newParagraph || newLetter || newItem ) {
			
			if( !paragraphId.equals("")) {
			
				p = paragraphId;
			}
		}
		
		if( newLetter || newItem ) {
		
			if( !letterId.equals("")) {
			
				l = letterId;
			}
		}
		
		if( newItem ) {
		
			if( !itemId.equals("")) {
			
				i = itemId;
			}
		}
	}	
	
	
	private void saveUp() {
	
		partitionText = yytext(); 
		offset += yylength();
		
		readPartitionIdUp(partitionsText);
		
		if(debug) System.out.println("addPartition() a:"+a+" p:"+p+" l:"+l+" i:"+i);
		addPartition(a, p, l, i, partitionText);
		 
		partitionsText = partitionsText.substring(yylength()); 
	}
	

	private static String ITEM = "[BOECLI:ITEM:";
	private static String LETTER = "[BOECLI:LETTER:";
	private static String PARAGRAPH = "[BOECLI:PARAGRAPH:";
	private static String ARTICLE = "[BOECLI:ARTICLE:";

	private void readPartitionIdUp(String text) {
		
		int itemStart = text.indexOf(ITEM);
		int letterStart = text.indexOf(LETTER);
		int paragraphStart = text.indexOf(PARAGRAPH);
		int articleStart = text.indexOf(ARTICLE);
		
		if(itemStart > -1) {
			
			i = text.substring(itemStart + ITEM.length(), text.substring(itemStart).indexOf("]") + itemStart);
		}
		
		if(letterStart > -1) {
			
			l = text.substring(letterStart + LETTER.length(), text.substring(letterStart).indexOf("]") + letterStart);
		}
		
		if(paragraphStart > -1) {
			
			p = text.substring(paragraphStart + PARAGRAPH.length(), text.substring(paragraphStart).indexOf("]") + paragraphStart);
		}
		
		if(articleStart > -1) {
			
			a = text.substring(articleStart + ARTICLE.length(), text.substring(articleStart).indexOf("]") + articleStart);
		}
		
		itemId = "";
		letterId = "";
		paragraphId = "";
		articleId = "";
		
	}


%} 



/* Partition Separator */
PartSep = ({SCDHE}|{BE_STOP})+ 

/* Lists are: ITEM or ITEM and ITEM or ITEM, ITEM, ITEM... or ITEM, ITEM, ITEM... and ITEM */
/* Lists can be ascending or descending in hierarchy */


ItemList = {BE_ITEM}(({SCE}{BE_ITEM})+)?({SCDHE}?{BE_STOP_AND}{SE}?{BE_ITEM})?

LetterUp = ({ItemList}{PartSep})?{BE_LETTER} 
LetterDown = {BE_LETTER}({PartSep}{ItemList})?

LetterListUp = {LetterUp}(({SCE}{LetterUp})+)?({SCDHE}?{BE_STOP_AND}{SE}?{LetterUp})?
LetterListDown = {LetterDown}(({SCE}{LetterDown})+)?({SCDHE}?{BE_STOP_AND}{SE}?{LetterDown})?

ParagraphUpLetter = ({LetterListUp}{PartSep})?{BE_PARAGRAPH}
ParagraphDownLetter = {BE_PARAGRAPH}({PartSep}{LetterListDown})?

ParagraphUpItem = ({ItemList}{PartSep})?{BE_PARAGRAPH}
ParagraphDownItem = {BE_PARAGRAPH}({PartSep}{ItemList})?

ParagraphUp = {ParagraphUpLetter}|{ParagraphUpItem}
ParagraphDown = {ParagraphDownLetter}|{ParagraphDownItem}

ParagraphListUp = {ParagraphUp}(({SCE}{ParagraphUp})+)?({SCDHE}?{BE_STOP_AND}{SE}?{ParagraphUp})?
ParagraphListDown = {ParagraphDown}(({SCE}{ParagraphDown})+)?({SCDHE}?{BE_STOP_AND}{SE}?{ParagraphDown})?

ArticleUpPar = {ParagraphListUp}{PartSep}{BE_ARTICLE}
ArticleDownPar = {BE_ARTICLE}{PartSep}{ParagraphListDown}

ArticleUpLett = {LetterListUp}{PartSep}{BE_ARTICLE}
ArticleDownLett = {BE_ARTICLE}{PartSep}{LetterListDown}

ArticleUpItem = {ItemList}{PartSep}{BE_ARTICLE}
ArticleDownItem = {BE_ARTICLE}{PartSep}{ItemList}

ArticleUp = {ArticleUpPar}|{ArticleUpLett}|{ArticleUpItem}
ArticleDown = {ArticleDownPar}|{ArticleDownLett}|{ArticleDownItem}

ArticleListUp1 = {ArticleUp}({SCE}{ArticleUp})*
ArticleListUp2 = {ArticleUp}{SCDHE}?{BE_STOP_AND}{SE}?{ArticleUp}
ArticleListUp3 = {ArticleUp}({SCE}{ArticleUp})+{SCDHE}?{BE_STOP_AND}{SE}?{ArticleUp}
ArticleListUp = {ArticleListUp1}|{ArticleListUp2}|{ArticleListUp3}

ArticleListDown1 = {ArticleDown}({SCE}{ArticleDown})*
ArticleListDown2 = {ArticleDown}{SCDHE}?{BE_STOP_AND}{SE}?{ArticleDown}
ArticleListDown3 = {ArticleDown}({SCE}{ArticleDown})+{SCDHE}?{BE_STOP_AND}{SE}?{ArticleDown}
ArticleListDown = {ArticleListDown1}|{ArticleListDown2}|{ArticleListDown3}



%x ArticleDownState ArticleUpState ReadDown ReadItem ReadLetter ReadParagraph ReadArticle     


%%



{ArticleListUp}|
{BE_ARTICLE}		{ 
						
						a = "";
						p = "";
						l = "";
						i = "";
						
						itemId = "";
						letterId = "";
						paragraphId = "";
						articleId = "";
						
						if(debug) System.out.println("in ALU");
						partitionsText = yytext(); 
						length = yylength(); 
						yypushback(length); 
						offset = 0; 
						yybegin(ArticleUpState); 
					}


<ArticleUpState> {

	{BE_ITEM}({PartSep}{BE_LETTER}({PartSep}{BE_PARAGRAPH}({PartSep}{BE_ARTICLE})?)?)? { if(debug) System.out.println("ALU->ILPA"); saveUp(); }
	
	{BE_ITEM}({PartSep}{BE_PARAGRAPH}({PartSep}{BE_ARTICLE})?)? { if(debug) System.out.println("ALU->IPA"); saveUp(); }
	
	{BE_LETTER}({PartSep}{BE_PARAGRAPH}({PartSep}{BE_ARTICLE})?)? { if(debug) System.out.println("ALU->LPA"); saveUp(); }
						
	{BE_PARAGRAPH}({PartSep}{BE_ARTICLE})? { if(debug) System.out.println("ALU->PA"); saveUp(); }
	
	{BE_ARTICLE} { if(debug) System.out.println("ALU->A"); saveUp(); }

	{BE_ITEM}({PartSep}{BE_LETTER}({PartSep}{BE_ARTICLE})?)? { if(debug) System.out.println("ALU->ILA"); saveUp(); }
	
	{BE_LETTER}({PartSep}{BE_ARTICLE})? { if(debug) System.out.println("ALU->LA"); saveUp(); }
	
	{BE_ITEM}({PartSep}{BE_ARTICLE})? { if(debug) System.out.println("ALU->IA"); saveUp(); }
						
	[^] {
			offset++;
			
			if( offset >= length ) {
				
				if( offset > length ) {
					yypushback(1);
				}
				
				yybegin(YYINITIAL);					
			}
			
			addText(yytext());
		}
}




{ArticleListDown}	{ 
						a = "";
						p = "";
						l = "";
						i = "";
						
						itemId = "";
						letterId = "";
						paragraphId = "";
						articleId = "";
						
						if(debug) System.out.println("in ALD");
						length = yylength(); 
						yypushback(length); 
						offset = 0; 
						yybegin(ArticleDownState); 
					}

<ArticleDownState> {

	{BE_ARTICLE}({PartSep}{BE_PARAGRAPH}({PartSep}{BE_LETTER}({PartSep}{BE_ITEM})?)?)? { if(debug) System.out.println("ALD->APLI"); saveDown(); }
	
	{BE_ARTICLE}({PartSep}{BE_PARAGRAPH}({PartSep}{BE_ITEM})?)? { if(debug) System.out.println("ALD->API"); saveDown(); }
	
	{BE_PARAGRAPH}({PartSep}{BE_LETTER}({PartSep}{BE_ITEM})?)? { if(debug) System.out.println("ALD->PLI"); saveDown(); }
	
	{BE_LETTER}({PartSep}{BE_ITEM})? { if(debug) System.out.println("ALD->LI"); saveDown(); }
	
	{BE_ITEM} { if(debug) System.out.println("ALD->I"); saveDown(); }
	
	{BE_ARTICLE}({PartSep}{BE_LETTER}({PartSep}{BE_ITEM})?)? { if(debug) System.out.println("ALD->ALI"); saveDown(); }
	
	{BE_ARTICLE}({PartSep}{BE_ITEM})? { if(debug) System.out.println("ALD->AI"); saveDown(); }
		
	[^] {
			offset++;
			
			if( offset >= length ) {
				
				if( offset > length ) {
					yypushback(1);
				}
				
				yybegin(YYINITIAL);					
			}
			
			addText(yytext());
		}
}


<ReadDown> {
	
	{BE_ARTICLE}	{ if(debug) System.out.println("RD->A"); newArticle = true; readOffset += yylength(); yypushback(yylength()); yybegin(ReadArticle); }
	
	{BE_PARAGRAPH}	{ if(debug) System.out.println("RD->P"); newParagraph = true; readOffset += yylength(); yypushback(yylength()); yybegin(ReadParagraph); }
		
	{BE_LETTER}		{ if(debug) System.out.println("RD->L"); newLetter = true; readOffset += yylength(); yypushback(yylength()); yybegin(ReadLetter); }
	
	{BE_ITEM}		{ if(debug) System.out.println("RD->I"); newItem = true; readOffset += yylength(); yypushback(yylength()); yybegin(ReadItem); }
	
	[^] {
			readOffset++;
			
			if( readOffset >= readLength ) {
				
				if( readOffset > readLength ) {
					yypushback(1);
				}
				
				readPartitionIdDown();
				
				if(debug) System.out.println("addPartition() a:"+a+" p:"+p+" l:"+l+" i:"+i);
				addPartition(a, p, l, i, partitionText);
				
				newArticle = false;
				newParagraph = false;
				newLetter = false;
				newItem = false;
				
				yybegin(ArticleDownState);					
			}
		}
}


<ReadItem> {
	
	{BE_ITEM_OPEN}	{ }
	[^\[\]]+		{ itemId = yytext(); }
	[\]]{BE_BODY}	{ yybegin(ReadDown); }
	[^]				{ yybegin(ReadDown); }
}

<ReadLetter> {
	
	{BE_LETTER_OPEN}	{ }
	[^\[\]]+			{ letterId = yytext(); }
	[\]]{BE_BODY}		{ yybegin(ReadDown); }
	[^]					{ yybegin(ReadDown); }
}

<ReadParagraph> {
	
	{BE_PARAGRAPH_OPEN}	{ }
	[^\[\]]+			{ paragraphId = yytext(); }
	[\]]{BE_BODY}		{ yybegin(ReadDown); }
	[^]					{ yybegin(ReadDown); }
}

<ReadArticle> {
	
	{BE_ARTICLE_OPEN}	{ }
	[^\[\]]+			{ articleId = yytext(); }
	[\]]{BE_BODY}		{ yybegin(ReadDown); }
	[^]					{ yybegin(ReadDown); }
}


[^]    		{ addText(yytext()); }





