/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.service.impl;

import java.io.IOException;
import java.io.StringReader;

public class Util {

	
	public static final String removeAllAnnotations(String text) {
		
		Cleaner ac = new Cleaner();
		
		try {
			
			ac.yyreset(new StringReader(text));
			
			ac.yylex();
			
		} catch (IOException e) {

			e.printStackTrace();
			
			return "";
		}
		
		return ac.getOutput();
	}
	
	public static final boolean isFutureReference(int year, String metaYear) {
		
		if(metaYear.length() < 1) {
			return false;
		}
		
		int value = 0;
		
		try {
			
			value = Integer.valueOf(metaYear);
			
		} catch (NumberFormatException e) {

			return false;
		}
	
		if(metaYear.length() == 2) {
			
			if(value < 21) {
				
				metaYear = "20" + metaYear;
				
			} else {
				
				metaYear = "19" + metaYear;
			}
		}
		
		value = Integer.valueOf(metaYear);
		
		if(year > value) {
			
			return true;
		}
		
		return false;
	}

	
	public static String getEqualPart(String a, String b) {
		
		for(int i = 0; i < a.length(); i++) {
			
			Character ai = a.charAt(i);
			
			if(i >= b.length()) {
				
				return a.substring(0, i);
			}
			
			Character bi = b.charAt(i);

			if( !ai.equals(bi)) {
				
				return a.substring(0, i);
			}
		}
		
		return a;
	}
	
	public static String normalizeYear(String year) {
		
		if(year.length() == 2) {

			int value = Integer.valueOf(year);

			if(value < 21) {
				
				year = "20" + year;
				
			} else {
				
				year = "19" + year;
			}
		
		}
		
		return year;
	}
}
