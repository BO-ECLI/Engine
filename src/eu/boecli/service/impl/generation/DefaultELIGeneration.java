/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Tommaso Agnoloni (ITTIG/CNR), Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.service.impl.generation;

import java.util.ArrayList;
import java.util.Collection;

import eu.boecli.common.CommonIdentifiers;
import eu.boecli.common.CommonLegislationTypes;
import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.engine.EngineDocument;
import eu.boecli.reference.LegalIdentifier;
import eu.boecli.reference.LegalIdentifierFactory;
import eu.boecli.reference.LegalReference;
import eu.boecli.service.generation.IdentifierGenerationService;

public class DefaultELIGeneration extends IdentifierGenerationService {

	
	/*
	 * Default implementation for generating ELI identifiers 
	 * of European legislation.
	 */
	

	@Override
	public Language language() {

		return Language.DEFAULT;
	}

	@Override
	public Jurisdiction jurisdiction() {

		return Jurisdiction.DEFAULT;
	}

	@Override
	public String serviceName() {

		return "Default ELI Generation";
	}

	@Override
	public String extensionName() {

		return "Default Extension";
	}

	@Override
	public String version() {

		return "0.1";
	}

	@Override
	public String author() {

		return "ITTIG";
	}
	

	@Override
	protected Collection<LegalIdentifier> getLegalIdentifiers(LegalReference legalReference) {
		

		Collection<LegalIdentifier> legalIdentifiers = new ArrayList<LegalIdentifier>();
		LegalIdentifier legalIdentifier = null;


		// Check http://publications.europa.eu/mdr/eli/documentation/uri_templates.html
		
		// TODO 
		
		// CONSOLIDATED EU legislation
		/*
		 * Consolidated legislation: http://data.europa.eu/eli/{typedoc}/{year}/{naturalnumber}/{start-date} with typedoc the resource type of the consolidated 
		 * legislation. start-date indicates the day of entry into force of the last amendment introduced in the consolidation in ISO format, 
		 * e.g. http://data.europa.eu/eli/dec/2009/496/2012-07-12.
		 * 
		 * If there is no consolidated version on the date, the nearest consolidation in the past to the date given will be returned, 
		 * or the base act if no consolidation exists yet. If the date given is before the date of publication of the base act, an error is returned.
		 */
		
		// PARTITIONS
		/*
		 * 
		 * Subdivisions
		 * In all of the templates one or more subdivisions can be inserted after the natural number to indicate the subdivisions of the text 
		 * in the form of http://data.europa.eu/eli/{typedoc}/{year}/{naturalnumber}{/subdivision*}/oj (i.e. the subdivision element can occur multiple times). 
		 * This can be for example a given article or article and paragraph such as http://data.europa.eu/eli/dir/2000/31/art_1/par_2/oj.
		 * 
		 * Note that at this stage the URIs will still return the full act. Developments to resolve the URIs to the appropriate subdivision are planned.
		 * 
		 */

		// LANGUAGE VARIANTS
		/*
		 * Language variants or specific file formats
		 * All of these templates can be used in combination 
		 * with a {language} or {language}/{format}, 
		 * e. g. http://data.europa.eu/eli/dir/2000/31/oj/fra or http://data.europa.eu/eli/dir/2000/31/oj/fra/pdf.
		 * Select language: bul spa ces dan deu est ell eng fra gle hrv ita lav lit hun mlt nld pol por ron slk slv fin swe
		 */
		
		
		String typeDoc = "";
		String prefix = "http://data.europa.eu/eli";
		String eli = "";

		String lang = ((EngineDocument) getEngineDocument()).getLanguage().toString();
		
		switch(lang) {

			case "IT":
				lang ="ita";
				break;
			case "ES":
				lang ="spa";
				break;
			case "EN":
				lang ="eng";
				break;
			case "NL":
				lang ="nld";
				break;
			case "DE":
				lang ="deu";
				break;
			case "FR":
				lang ="fra";
				break;
			case "RO":
				lang ="ron";
				break;
		}
		
		
		double confidence = 0.1;


		
		if( !legalReference.isLegislationReference()) {
			return null;

		}


		if(legalReference.getType().equalsIgnoreCase(CommonLegislationTypes.DIRECTIVE.toString())) { typeDoc = "dir";  confidence = 0.7; }
		if(legalReference.getType().equalsIgnoreCase(CommonLegislationTypes.REGULATION.toString())) { typeDoc = "reg"; confidence = 0.7; }
		if(legalReference.getType().equalsIgnoreCase(CommonLegislationTypes.DECISION.toString())) { typeDoc = "dec"; confidence = 0.7; }
		

		if(legalReference.getAuthority() != null && legalReference.getAuthority().startsWith("EU"))  {confidence=0.9;}



		if(!typeDoc.equalsIgnoreCase("")){

			String number = legalReference.getNumber();
			String year = legalReference.getYear();

			// partitions
			// ex Partition: article-44-bis;paragraph-1;
			String artValue = "";
			String artPrefix = "article-";
			
			String parValue = "";
			String parPrefix ="paragraph-"; 
			
			String eliPartition="";
			if( !legalReference.getPartition().equals("")) {
				
				// article
				int artStart = legalReference.getPartition().indexOf(artPrefix);
				if(artStart!=-1){
					int artEnd = legalReference.getPartition().indexOf(";", artStart);
					artValue = legalReference.getPartition().substring(artStart + artPrefix.length(), artEnd);
					artValue = artValue.replace("-", "");
					if(!artValue.equals("")) eliPartition+="/art_"+artValue;
				}
				// paragraph
				int parStart = legalReference.getPartition().indexOf(parPrefix);
				if(parStart!=-1){
					int parEnd = legalReference.getPartition().indexOf(";", parStart);
					parValue = legalReference.getPartition().substring(parStart + parPrefix.length(), parEnd);	
					parValue = parValue.replace("-", "");
					if(!parValue.equals("")) eliPartition+="/par_"+parValue;
				}		
			}
			
			
			
			eli =  prefix+"/"+typeDoc+"/"+year+"/"+number+eliPartition+"/oj";
			String url = eli+"/"+lang;


			legalIdentifier = LegalIdentifierFactory.createLegalIdentifier(CommonIdentifiers.ELI, eli);
			if(legalIdentifier != null) {
				legalIdentifier.setUrl(url);
				legalIdentifier.setConfidence(confidence);
				legalIdentifiers.add(legalIdentifier);
			}
		}

		return legalIdentifiers;
	}
}

