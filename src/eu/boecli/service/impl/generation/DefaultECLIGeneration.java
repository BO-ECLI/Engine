/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Tommaso Agnoloni (ITTIG/CNR), Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.service.impl.generation;

import java.util.ArrayList;
import java.util.Collection;

import eu.boecli.common.CommonCaseLawAuthorities;
import eu.boecli.common.CommonCaseLawTypes;
import eu.boecli.common.CommonIdentifiers;
import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.reference.LegalIdentifier;
import eu.boecli.reference.LegalIdentifierFactory;
import eu.boecli.reference.LegalReference;
import eu.boecli.service.generation.IdentifierGenerationService;

public class DefaultECLIGeneration extends IdentifierGenerationService {

	
	/*
	 * Default implementation for generating ECLI identifiers 
	 * of case-laws issued by European courts.
	 */
	
	public final static String PREFIX = "ECLI";
	
	public final static String SEPARATOR = ":";
	

	@Override
	public Language language() {

		return Language.DEFAULT;
	}

	@Override
	public Jurisdiction jurisdiction() {

		return Jurisdiction.DEFAULT;
	}

	@Override
	public String serviceName() {

		return "Default ECLI Generation";
	}

	@Override
	public String extensionName() {

		return "Default Extension";
	}
	
	@Override
	public String version() {

		return "0.3";
	}

	@Override
	public String author() {

		return "ITTIG";
	}
	

	// FIXME joined cases
	// Corte eur. dir. uomo, IV sez., 21 giugno 2016, Ramos Nunes de Carvalho e Sa c/ Portogallo, ric. nn. 55391/13, 57728/13 e 74041/13
	// ECLI:CE:ECHR:2016:0621JUD5539113;5772813;7404113",
	
	@Override
	protected Collection<LegalIdentifier> getLegalIdentifiers(LegalReference legalReference) {
		
		Collection<LegalIdentifier> legalIdentifiers = new ArrayList<LegalIdentifier>();

		LegalIdentifier legalIdentifier = null;

		if(legalReference.getAuthority() != null && legalReference.getAuthority().equals(CommonCaseLawAuthorities.ECHR.toString())) {

			
			String urlPrefix = "http://hudoc.echr.coe.int/eng#";
			String fullCaseNumber = "";
			String caseNumber = "";
			String date = "";
			String year = "";
			String typeDoc = "";
			boolean joinedCases = false;
			double conf = 0.1;
			
			
			fullCaseNumber = legalReference.getCaseNumber(true);
			caseNumber = fullCaseNumber;
			if(fullCaseNumber.indexOf(";")!=-1){
				// in joined cases the case number used in the ECLI identifier is the first 				
				caseNumber = fullCaseNumber.substring(0, fullCaseNumber.indexOf(";"));
				joinedCases = true;
			}
			
			year = legalReference.getYear();
			
			if(year.equals("")) {
				//Look in the date
				date = legalReference.getDate();
				if(date.length() > 7) {
					year = date.substring(0, 4);
				}
			}
			
			date = legalReference.getDate();
			typeDoc = legalReference.getType();

			
			// in this case ECLI can be composed automatically (apart from the documentType JUD/DEC/REP )
			if(!caseNumber.equals("") && !date.equals("")){
				
				caseNumber = caseNumber.replace("/", "");
				if(caseNumber.length()==4) caseNumber = "00000"+caseNumber;
				if(caseNumber.length()==5) caseNumber = "0000"+caseNumber;
				if(caseNumber.length()==6) caseNumber = "000"+caseNumber;
				if(caseNumber.length()==7) caseNumber = "00"+caseNumber;
				if(caseNumber.length()==8) caseNumber = "0"+caseNumber;
				date = date.substring(4, date.length()).replaceAll("-", "");
				
				if(legalReference.getType().equalsIgnoreCase(CommonCaseLawTypes.JUDGMENT.toString())) { typeDoc = "JUD"; conf = 0.9; }
				if(legalReference.getType().equalsIgnoreCase(CommonCaseLawTypes.DECISION.toString())) { typeDoc = "DEC"; conf = 0.9; }
				
					
				String code = "";
				String url = ""; 
				
				if(!typeDoc.equals("")){
					
					code = PREFIX+SEPARATOR+"CE"+SEPARATOR+"ECHR"+SEPARATOR+year+SEPARATOR+date+typeDoc+caseNumber;
					url = urlPrefix+"{%22ecli%22:[%22"+code+"%22]}";
					
					if(joinedCases) conf = 0.4;
					
					legalIdentifier = LegalIdentifierFactory.createLegalIdentifier(CommonIdentifiers.ECLI, code);
					if(legalIdentifier != null) {
						legalIdentifier.setUrl(url);
						legalIdentifier.setConfidence(conf);
						legalIdentifiers.add(legalIdentifier);
					}
				}
				else{  // ADDED THE TWO VARIANTS JUD/DEC with lowered confidence  
					
					// JUD version
					code = PREFIX+SEPARATOR+"CE"+SEPARATOR+"ECHR"+SEPARATOR+year+SEPARATOR+date+"JUD"+caseNumber;
					url = urlPrefix+"{%22ecli%22:[%22"+code+"%22]}";
					conf = 0.6;
					if(joinedCases) conf = 0.4;
					
					legalIdentifier = LegalIdentifierFactory.createLegalIdentifier(CommonIdentifiers.ECLI, code);
					if(legalIdentifier != null) {
						legalIdentifier.setUrl(url);
						legalIdentifier.setConfidence(conf);
						legalIdentifiers.add(legalIdentifier);
					}
					
					// DEC version
					code = PREFIX+SEPARATOR+"CE"+SEPARATOR+"ECHR"+SEPARATOR+year+SEPARATOR+date+"DEC"+caseNumber;
					url = urlPrefix+"{%22ecli%22:[%22"+code+"%22]}";
					conf = 0.4;
					if(joinedCases) conf = 0.2;
					
					
					legalIdentifier = LegalIdentifierFactory.createLegalIdentifier(CommonIdentifiers.ECLI, code);
					if(legalIdentifier != null) {
						legalIdentifier.setUrl(url);
						legalIdentifier.setConfidence(conf);
						legalIdentifiers.add(legalIdentifier);
					}
					
				}
			
			}
			
			
		}

		return legalIdentifiers;
	}
}
