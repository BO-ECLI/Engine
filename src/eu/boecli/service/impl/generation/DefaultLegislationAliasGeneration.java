/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Tommaso Agnoloni (ITTIG/CNR), Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.service.impl.generation;

import java.util.ArrayList;
import java.util.Collection;

import eu.boecli.common.CommonIdentifiers;
import eu.boecli.common.CommonLegislationAliases;
import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.engine.EngineDocument;
import eu.boecli.reference.LegalIdentifier;
import eu.boecli.reference.LegalIdentifierFactory;
import eu.boecli.reference.LegalReference;
import eu.boecli.service.generation.IdentifierGenerationService;

public class DefaultLegislationAliasGeneration extends IdentifierGenerationService {

	/*
	 * Default implementation for generating identifiers 
	 * of predefined common aliases.
	 */
	

	@Override
	public Language language() {

		return Language.DEFAULT;
	}

	@Override
	public Jurisdiction jurisdiction() {

		return Jurisdiction.DEFAULT;
	}

	@Override
	public String serviceName() {

		return "Default Legislation Alias Identifier Generation";
	}

	@Override
	public String extensionName() {

		return "Default Extension";
	}

	@Override
	public String version() {

		return "0.2";
	}

	@Override
	public String author() {

		return "ITTIG";
	}
	
	
	// CHECK http://eur-lex.europa.eu/collection/eu-law/treaties/treaties-overview.html
	
	@Override
	protected Collection<LegalIdentifier> getLegalIdentifiers(LegalReference legalReference) throws IllegalArgumentException {

		Collection<LegalIdentifier> legalIdentifiers = new ArrayList<LegalIdentifier>();
		
		LegalIdentifier legalIdentifier = null;
		
		int inputYear = 0;
		
		try {
			
			inputYear = Integer.valueOf(((EngineDocument) getEngineDocument()).getInputYear());
			
		} catch (Exception e) {
			inputYear = 0;
		}
		

		String artValue = "";
		String artPrefix = "article-";
		if( !legalReference.getPartition().equals("")) {
			int artStart = legalReference.getPartition().indexOf(artPrefix);
			int artEnd = legalReference.getPartition().indexOf(";", artStart);
			int dash = legalReference.getPartition().indexOf("-", artStart + artPrefix.length());
			if(dash > -1 && dash < artEnd) {
				artEnd = dash;
			}
			artValue = legalReference.getPartition().substring(artStart + artPrefix.length(), artEnd);
		}
		
	
	
		
		String lang = ((EngineDocument) getEngineDocument()).getLanguage().toString();
		
        String prefix = "http://eur-lex.europa.eu/legal-content/" + lang + "/TXT/?uri=CELEX:";
        String celex = "";
        double confidence = 0.7;
    	String celexSuffix = "";
    	
		if(artValue.length()>0){
			if(artValue.length()==1) artValue ="00"+artValue;
			if(artValue.length()==2) artValue ="0"+artValue;
			celexSuffix = artValue;
		}else
			celexSuffix = "/TXT";

		
		
		switch(CommonLegislationAliases.valueOf(legalReference.getAliasValue())) {
		
			case TREATY_EU : case TREATY_MAASTRICHT:
					
				/* 
				 * Treaty on European union (Consolidated version 2016)
				 * celex = 12016M/TXT
				 * 
				 * Treaty on European Union (Consolidated version 2012)
				 * celex = 12012M/TXT
				 * 
				 * Treaty on European Union (Consolidated version 2010)	
				 * celex = 12010M/TXT
				 * 
				 * Treaty on European Union (Consolidated version 2008)	
				 * celex = 12008M/TXT
				 * 
				 * Treaty on European Union (Consolidated version 2006)	
				 * celex = 12006M/TXT
				 * 
				 * Treaty on European Union (Consolidated version 2002)	
				 * celex = 12002M/TXT
				 * 
				 * Treaty on European Union (Consolidated version 1997)	
				 * celex = 11997M/TXT
				 * 
				 * Treaty on European Union (1992)		
				 * celex = 11992M/TXT
				 * 
				 */

				celex = "11992M";
				
				if(inputYear > 0) {	
					if(inputYear > 1997) celex = "11997M";	
					if(inputYear > 2002) celex = "12002M";
					if(inputYear > 2006) celex = "12006M";
					if(inputYear > 2008) celex = "12008M";
					if(inputYear > 2010) celex = "12010M";
					if(inputYear > 2012) celex = "12012M";
					if(inputYear > 2016) celex = "12016M";
				}
				
				break;

			case TREATY_FUNCTIONING_EU :
				
				/*
				 * Treaty on the Functioning of the European Union (Consolidated version 2016)
				 * celex = 12016E/TXT
				 * 
				 * Treaty on the Functioning of the European Union (Consolidated version 2012)
				 * celex = 12012E/TXT
				 * 
				 * 
				 * Treaty on the Functioning of the European Union (Consolidated version 2010)	
				 * celex = 12010E/TXT
				 * 
				 * Treaty on the Functioning of the European Union (Consolidated version 2008)	
				 * celex = 12008E/TXT
				 * 
				 */
				
				celex = "12008E";
				
				if(inputYear > 0) {
					
					if(inputYear > 2010) celex = "12010E";
					if(inputYear > 2012) celex = "12012E";
					if(inputYear > 2016) celex = "12016E";

				}
				
				break;
				
				
			case MERGER_TREATY:
				
				/*
				 * Treaty amending certain budgetary provisions (1970)		
				 * celex = 11970F/TXT
				 * 
				 * Merger Treaty (1965)		
				 * celex = 11965F/TXT
				 */
				
				celex = "11965F";
				
				if(inputYear > 0) {
					
					if(inputYear > 1970) celex = "11970F";
					
				}
				
				break;
				
	
				
			case TREATY_EU_ATOMIC_ENERGY_COMMUNITY :
				
				/*
				 * Treaty establishing the European Atomic Energy Community (Consolidated version 2016)
				 * celex = 12016A/TXT
				 * 
				 * Treaty establishing the European Atomic Energy Community (Consolidated version 2012)
				 * celex = 12012A/TXT
				 * 
				 * Treaty establishing the European Atomic Energy Community (Consolidated version 2010)
				 * celex = 12010A/TXT
				 * 
				 * Treaty establishing the European Atomic Energy Community (1957)	
				 * celex = 11957A/TXT
				 * 
				 */
				
				celex = "11957A";
				
				if(inputYear > 0) {
					if(inputYear > 2010) celex = "12010A";
					if(inputYear > 2012) celex = "12012A";
					if(inputYear > 2016) celex = "12016A";
				}
				
				break;
				
				
			case TREATY_LISBON :
				
				/*
				 * Treaty of Lisbon (2007)	
				 * celex = 12007L/TXT
				 * 
				 * 
				 */
				
				celex = "12007L";
				
				break;
				
			case TREATY_EU_COMMUNITY :
				
				/*
				 * Treaty establishing the European Community (Consolidated version 2006)	
				 * celex = 12006E/TXT
				 * 
				 * Treaty establishing the European Community (Consolidated version 2002)	
				 * celex = 12002E/TXT
				 * 
				 * Treaty establishing the European Community (Consolidated version 1997)	
				 * celex = 11997E/TXT
				 * 
				 * Treaty establishing the European Community (Consolidated version 1992)	
				 * celex = 11992E/TXT
				 * 
				 */
				
				celex = "11992E";
				
				if(inputYear > 0) {
					if(inputYear > 1997) celex = "11997E";
					if(inputYear > 2002) celex = "12002E";
					if(inputYear > 2006) celex = "12006E";
				}
				
				break;
				
				
			case TREATY_EU_CONSTITUTION :
				
				/*
				 * Treaty establishing a Constitution for Europe (2004)	
				 * celex = 12004V/TXT
				 */
				
				celex = "12004V";
				
				confidence = 0.7;
				
				break;

			case TREATY_EEC :
				
				/*
				 * Treaty establishing the European Economic Community (1957)	
				 * celex = 11957E/TXT
				 */
				
				celex = "11957E";
				
				break;
				
			case TREATY_EU_COAL_STEEL :
				
				/*
				 * Treaty establishing the European Coal and Steel Community (1951)		
				 * celex = 11951K/TXT
				 */
				
				celex = "11951K";
				
				confidence = 0.7;
				
				break;

				
				
			case TREATY_NICE :
				
				/*
				 * Treaty of Nice (2001)	
				 * celex = 12001C/TXT
				 */
				
				celex = "12001C";
				
				break;

				
			case TREATY_AMSTERDAM :
				
				/*
				 * Treaty of Amsterdam (1997)	
				 * celex = 11997D/TXT
				 */
				
				celex = "11997D";
				
				break;
		
				
			case EU_CHARTER :
				
				/*
				 * 
				 * Charter of Fundamental Rights of the European Union (2012)
				 * celex = 12012P/TXT
				 * 
				 * Charter of Fundamental Rights of the European Union (2010)
				 * celex = 12010P/TXT
				 * 
				 * Charter of Fundamental Rights of the European Union (2007)
				 * celex = 12007P/TXT
				 * 
				 * 
				 */
				
				celex = "12007P";
				
				if(inputYear > 0) {
					if(inputYear > 2010) celex = "12010P";
					if(inputYear > 2012) celex = "12012P";
				}
				
				break;

		}
		
		
		if(celex.length()>0){
			celex = celex +celexSuffix;
			legalIdentifier = LegalIdentifierFactory.createLegalIdentifier(CommonIdentifiers.CELEX, celex);
			if(legalIdentifier != null) {
				legalIdentifier.setUrl(prefix+celex);
				legalIdentifier.setConfidence(confidence);
				legalIdentifiers.add(legalIdentifier);
			}
		}
		

		return legalIdentifiers;
	}

}
