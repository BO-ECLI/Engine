/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Tommaso Agnoloni (ITTIG/CNR), Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.service.impl.generation;

import java.util.ArrayList;
import java.util.Collection;

import eu.boecli.common.CommonIdentifiers;
import eu.boecli.common.CommonLegislationTypes;
import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.engine.EngineDocument;
import eu.boecli.reference.LegalIdentifier;
import eu.boecli.reference.LegalIdentifierFactory;
import eu.boecli.reference.LegalReference;
import eu.boecli.service.generation.IdentifierGenerationService;


public class DefaultCELEXGeneration extends IdentifierGenerationService {


	/*
	 * Default implementation for generating CELEX identifiers 
	 * of European legislation.
	 */

	public final static String PREFIX = "CELEX";
	
	public final static String SEPARATOR = ":";
	
	//http://eur-lex.europa.eu/legal-content/IT/TXT/?uri=CELEX:11957E226

	@Override
	public Language language() {

		return Language.DEFAULT;
	}

	@Override
	public Jurisdiction jurisdiction() {

		return Jurisdiction.DEFAULT;
	}

	@Override
	public String serviceName() {

		return "Default CELEX Generation";
	}
	
	@Override
	public String extensionName() {

		return "Default Extension";
	}

	@Override
	public String version() {

		return "0.2";
	}

	@Override
	public String author() {

		return "ITTIG";
	}

	@Override
	protected final Collection<LegalIdentifier> getLegalIdentifiers(LegalReference legalReference) {

		
		Collection<LegalIdentifier> legalIdentifiers = new ArrayList<LegalIdentifier>();

		LegalIdentifier legalIdentifier = null;


		String type = "";
		String sector = "";

		String lang = ((EngineDocument) getEngineDocument()).getLanguage().toString();	
        String prefix = "http://eur-lex.europa.eu/legal-content/" + lang + "/TXT/?uri=CELEX:";
		
		String celex = "";

		double confidence = 0.1;


		// WARNING:   CELEX might also be composed for case-law 
		if( !legalReference.isLegislationReference()) {
			return null;

			/*
			 * CELEX CJEU (Sector 6 - Case law) Pre Lisbon / Post Lisbon 
			 * 
			 *  richiamate le sentenze 31 marzo 1993, C-19/92; 30 novembre 1995, C-55/94), 
			 *  
			 *  http://eur-lex.europa.eu/legal-content/IT/TXT/HTML/?uri=CELEX:61992CJ0019&from=IT
			 *  
			 *  http://eur-lex.europa.eu/legal-content/IT/TXT/PDF/?uri=CELEX:61994CJ0055&rid=3
			 *  
			 */
		}


		if(legalReference.getType().equalsIgnoreCase(CommonLegislationTypes.DIRECTIVE.toString())) { sector="3"; type = "L";  confidence = 0.7; }
		if(legalReference.getType().equalsIgnoreCase(CommonLegislationTypes.REGULATION.toString())) { sector="3"; type = "R"; confidence = 0.7; }
		if(legalReference.getType().equalsIgnoreCase(CommonLegislationTypes.RECOMMENDATION.toString())) { sector="3"; type = "H"; confidence = 0.7; }
		if(legalReference.getType().equalsIgnoreCase(CommonLegislationTypes.DECISION.toString())) { sector="3"; type = "D"; confidence = 0.7;  }

		if(legalReference.getAuthority() != null && legalReference.getAuthority().startsWith("EU"))  {confidence=0.9;}


		
		if(!type.equalsIgnoreCase("")){
			
			String number = legalReference.getNumber();
			if(number.length()==1) number = "000"+number;
			if(number.length()==2) number = "00"+number;
			if(number.length()==3) number = "0"+number;
			
			
			String year = legalReference.getYear();
				
			celex =  sector+year+type+number;

			String url = prefix + celex;


			legalIdentifier = LegalIdentifierFactory.createLegalIdentifier(CommonIdentifiers.CELEX, celex);
			if(legalIdentifier != null) {
				legalIdentifier.setUrl(url);
				legalIdentifier.setConfidence(confidence);	
				legalIdentifiers.add(legalIdentifier);
			}

		}



		/*
		String sector = ""; //1-digit

		String year = ""; //4-digits

		String descriptor = ""; //1-letter or 2-letters

		String number = ""; //4-digits

		String article = ""; //3-digits  

		String annex = ""; //"N" + 2-digits
		 */

		//article letter? Article G //1-char //i.e.: 11992MG/B8: Treaty on European Union, Title II: Provisions amending the Treaty establishing the European Economic Community with a view to establishing the European Community, Article G – Article G B 8

		//Sub-partitions? i.e.: 11997D001/P9: Treaty of Amsterdam amending the Treaty on European Union, the Treaties establishing the European Communities and certain related acts, Part One: Substantive amendments, Article 1.9


		/*
		 *  CELEX  EU Legislation (Sector 3) 

			R
			Regulations
			32009R1290: Commission Regulation (EU) No 1290/2009 of 23 December 2009 fixing the import duties in the cereals sector applicable from 1 January 2010
			32001R0469: Council Regulation (EC) No 469/2001 of 6 March 2001 imposing a definitive anti-dumping duty on imports of certain electronic weighing scales originating in Singapore

			L
			Directives
			32009L0147: Directive 2009/147/EC of the European Parliament and of the Council of 30 November 2009 on the conservation of wild birds 31963L0340: Council Directive 63/340/EEC of 31 May 1963 on the abolition of all prohibitions on or obstacles to payments for services where the only restrictions on exchange of services are those governing such payments

		 */




		return legalIdentifiers;



	}

}
