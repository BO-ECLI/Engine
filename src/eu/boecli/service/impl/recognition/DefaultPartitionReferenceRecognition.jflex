/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.service.impl.recognition;

import java.io.IOException;
import java.io.StringReader;

import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.service.recognition.ReferenceRecognitionService;


%%
%class DefaultPartitionReferenceRecognition 
%extends ReferenceRecognitionService
%standalone
%unicode
%caseless
%char
%line
%column
%public

%include ../StdMacros.in
%include ../BoecliMacros.in

%{
	
	/* Custom java code */


	@Override
	public Language language() { return Language.DEFAULT; }
	
	@Override
	public Jurisdiction jurisdiction() { return Jurisdiction.DEFAULT; }

	@Override
	public String serviceName() { return "Generic partitions recognition for legislation references"; }
	
	@Override
	public String extensionName() { return "Default Extension"; }

	@Override
	public String version() { return "0.4"; }
	
	@Override
	public String author() { return "ITTIG"; }
	

	/* An empty default constructor is required to comply with BOECLIService */
	
	public DefaultPartitionReferenceRecognition() { }
	
	@Override
	public final boolean run() {
			
		try {
			
			yyreset(new StringReader(getEngineDocument().getAnnotatedText()));
			
			yylex();
			
			
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	private int allLength = 0;
	private int allOffset = 0;
	
	private int oneLength = 0;
	private int oneOffset = 0;
	
	private String context = "";

%} 



BE_PARTS = {BE_PART}({BE_SEP}?{BE_PART})*

PART_REF = ({BE_PARTS}{BE_SEP}?)?{BE_LEGAL_REF}

REF_PART = {BE_LEGAL_REF}({BE_SEP}?{BE_PARTS})?



%x readPartRefs readRefParts readPartRef readRefPart 

%% 



{PART_REF}({BE_SEP}?{PART_REF})*		{ allOffset = 0; allLength = yylength(); yypushback(allLength); yybegin(readPartRefs); }

{REF_PART}({BE_SEP}?{REF_PART})*		{ allOffset = 0; allLength = yylength(); yypushback(allLength); yybegin(readRefParts); }


<readPartRefs> {

	({BE_PARTS}{BE_SEP}?)?{BE_LEGAL_REF}	{ allOffset += yylength(); oneOffset = 0; oneLength = yylength(); context = yytext(); yypushback(oneLength); yybegin(readPartRef); }
		
	[^] {
	
		allOffset++;
		
		if(allOffset >= allLength) {
			
			if(allOffset > allLength) yypushback(1);
			
			yybegin(YYINITIAL);
		}
		
		addText(yytext());
	}
}

<readRefParts> {

	{BE_LEGAL_REF}({BE_SEP}?{BE_PARTS})?	{ allOffset += yylength(); oneOffset = 0; oneLength = yylength(); context = yytext(); yypushback(oneLength); yybegin(readRefPart); }
		
	[^] {
	
		allOffset++;
		
		if(allOffset >= allLength) {
			
			if(allOffset > allLength) yypushback(1);
			
			yybegin(YYINITIAL);
		}
		
		addText(yytext());
	}
}

<readPartRef> {

	{BE_PART}{BE_SEP}?{BE_LEGAL_REF}	{ 
											extendReference(yytext(), context);
											oneOffset += yylength(); 
										}
	
	{BE_PART}							{ 
											cloneReference(yytext(), context);
											oneOffset += yylength(); 
										}
	
	{BE_LEGAL_REF}						{ addText(yytext()); oneOffset += yylength(); }
	
	[^] {
	
		oneOffset++;
		
		if(oneOffset >= oneLength) {
			
			if(oneOffset > oneLength) yypushback(1);
			
			yybegin(YYINITIAL);
		}
		
		addText(yytext());
	}		
}


<readRefPart> {

	{BE_LEGAL_REF}{BE_SEP}?{BE_PART}	{
											extendReference(yytext(), context);
											oneOffset += yylength(); 
										}
	
	{BE_PART}							{ 
											cloneReference(yytext(), context);
											oneOffset += yylength(); 
										}
	
	{BE_LEGAL_REF}						{ addText(yytext()); oneOffset += yylength(); }
	
	[^] {
	
		oneOffset++;
		
		if(oneOffset >= oneLength) {
			
			if(oneOffset > oneLength) yypushback(1);
			
			yybegin(YYINITIAL);
		}
		
		addText(yytext());
	}
}



[^]		{ 
			addText(yytext()); 
		}



