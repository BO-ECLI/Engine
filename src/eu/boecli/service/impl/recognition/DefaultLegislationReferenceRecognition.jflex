/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.service.impl.recognition;

import java.io.IOException;
import java.io.StringReader;

import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.service.recognition.ReferenceRecognitionService;


%%
%class DefaultLegislationReferenceRecognition 
%extends ReferenceRecognitionService
%standalone
%unicode
%caseless
%char
%line
%column
%public

%include ../StdMacros.in
%include ../BoecliMacros.in

%{
	
	/* Custom java code */


	@Override
	public Language language() { return Language.DEFAULT; }
	
	@Override
	public Jurisdiction jurisdiction() { return Jurisdiction.DEFAULT; }

	@Override
	public String serviceName() { return "Generic reference recognition to legislation"; }
	
	@Override
	public String extensionName() { return "Default Extension"; }

	@Override
	public String version() { return "1.2"; }
	
	@Override
	public String author() { return "ITTIG"; }
	

	/* An empty default constructor is required to comply with BOECLIService */
	
	public DefaultLegislationReferenceRecognition() { }
	
	@Override
	public final boolean run() {
			
		try {
			
			yyreset(new StringReader(getEngineDocument().getAnnotatedText()));
			
			yylex();
			
			
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	private int length = 0;
	private int offset = 0;
	
	private StringBuilder pattern = new StringBuilder();
	private StringBuilder context = new StringBuilder();
	private StringBuilder buffer = new StringBuilder();
	
	private String patternContext = "";
	
	private boolean auth = false;
	private boolean type = false;
	private boolean alias = false;
	private boolean sect = false;
	private boolean date = false;
	private boolean num = false;
	private boolean altNum = false;

	private void reset(boolean full) {
	
		if(full) {
		
			auth = false;
			type = false;
			sect = false;
			alias = false;
			
			context = new StringBuilder();
			
			offset = 0; 
			length = yylength(); 
		}
		
		date = false;
		num = false;
		altNum = false;
		
		pattern = new StringBuilder();
		buffer = new StringBuilder();		
	}
	
	private void checkAndSave() {
		
		checkAndSave(false);
	}
	
	private void checkAndSave(boolean last) {
	
		if( (type && date) || (type && num) || (type && altNum) || (alias)  ) {
		
			String patternStr = pattern.toString().substring(0, pattern.length() - buffer.length());
			
			//System.out.println(">>>>>>>>> addReference text: " + patternStr + " - context: " + context.toString());
			
			//addReference(patternStr, context.toString());
			addReference(patternStr, patternContext);
			
			addText(buffer.toString());
			
			reset(false);
			
			return;
			
		}
		
		if(last || date || num || num || altNum ) {
				
			addText(pattern.toString());
			
			reset(false);
		}
	}
	
%} 


/* Generic patterns for legislation references. */

FEATURE = {BE_LEGISLATION_TYPE}|{BE_LEGISLATION_AUTH}|{BE_SECTION}|{BE_DATE}|{BE_NUMBER}|{BE_NUMBERYEAR}|{BE_ALTNUMBER}|{BE_GEO}|{BE_MISC}|{BE_ALIAS}

PATTERN = {FEATURE}({BE_SEP}?{FEATURE})+


%x readPattern  

%% 


{PATTERN}	{ reset(true); patternContext = yytext(); yypushback(length); yybegin(readPattern); }


<readPattern> {

	
	{BE_LEGISLATION_TYPE}	{ buffer = new StringBuilder(); offset += yylength(); if(type || alias) { checkAndSave(); } type = true; alias = false; pattern.append(yytext()); context.append(yytext()); }

	{BE_LEGISLATION_AUTH}	{ buffer = new StringBuilder(); offset += yylength(); if(auth || alias) { checkAndSave(); context = new StringBuilder(); } auth = true; alias = false; pattern.append(yytext()); context.append(yytext()); }
	
	{BE_SECTION}			{ buffer = new StringBuilder(); offset += yylength(); if(sect || alias) { checkAndSave(); } sect = true; alias = false; pattern.append(yytext()); context.append(yytext()); }
		
	{BE_DATE}				{ offset += yylength(); if(date || alias) { checkAndSave(); } date = true; alias = false; pattern.append(yytext()); context.append(yytext()); buffer = new StringBuilder(); }
	
	{BE_NUMBER}				{ offset += yylength(); if(num || alias) { checkAndSave(); } num = true; alias = false; pattern.append(yytext()); context.append(yytext()); buffer = new StringBuilder(); }

	{BE_NUMBERYEAR}			{ offset += yylength(); if(num || alias) { checkAndSave(); } num = true; alias = false; pattern.append(yytext()); context.append(yytext()); buffer = new StringBuilder(); }		
	
	{BE_ALTNUMBER}			{ offset += yylength(); if(altNum || alias) { checkAndSave(); } altNum = true; alias = false; pattern.append(yytext()); context.append(yytext()); buffer = new StringBuilder(); }

	{BE_GEO}				{ offset += yylength(); if(alias) { checkAndSave(); } alias = false; pattern.append(yytext()); context.append(yytext()); buffer = new StringBuilder(); }
	
	{BE_ALIAS}				{ offset += yylength(); alias = true; pattern.append(yytext()); context.append(yytext()); buffer = new StringBuilder(); }
							
	{BE_MISC}				{ offset += yylength(); pattern.append(yytext()); context.append(yytext()); buffer = new StringBuilder(); }
		
	
	[^] {
	
		offset++;
		
		if(offset >= length) {
			
			checkAndSave(true);
			
			if(offset > length) yypushback(1);
			
			yybegin(YYINITIAL);
		}
		
		pattern.append(yytext());
		context.append(yytext());
		buffer.append(yytext());
	}	
			
}



[^]		{ 
			addText(yytext()); 
		}



