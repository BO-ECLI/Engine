/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.service.impl.recognition;

import java.io.IOException;
import java.io.StringReader;

import eu.boecli.common.Jurisdiction;
import eu.boecli.common.Language;
import eu.boecli.service.recognition.ReferenceRecognitionService;


%%
%class DefaultCaseLawReferenceRecognition 
%extends ReferenceRecognitionService
%standalone
%unicode
%caseless
%char
%line
%column
%public

%include ../StdMacros.in
%include ../BoecliMacros.in

%{
	
	/* Custom java code */


	@Override
	public Language language() { return Language.DEFAULT; }
	
	@Override
	public Jurisdiction jurisdiction() { return Jurisdiction.DEFAULT; }

	@Override
	public String serviceName() { return "Generic reference recognition to case-law"; }
	
	@Override
	public String extensionName() { return "Default Extension"; }

	@Override
	public String version() { return "2.2"; }
	
	@Override
	public String author() { return "ITTIG"; }
	

	/* An empty default constructor is required to comply with BOECLIService */
	
	public DefaultCaseLawReferenceRecognition() { }
	
	@Override
	public final boolean run() {
			
		try {
			
			yyreset(new StringReader(getEngineDocument().getAnnotatedText()));
			
			yylex();
			
			
		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	private int length = 0;
	
	private int offset = 0;
	
	private StringBuilder pattern = new StringBuilder();
	private StringBuilder context = new StringBuilder();
	private StringBuilder buffer = new StringBuilder();
	
	private String patternContext = "";
	
	private boolean auth = false;
	private boolean type = false;
	private boolean alias = false;
	private boolean sect = false;
	private boolean subj = false;
	private boolean date = false;
	private boolean num = false;
	private boolean caseNum = false;
	private boolean altNum = false;
	private boolean appl = false;
	private boolean def = false;
	private boolean part = false;
	private boolean ecli = false;
	
	private boolean isPart = false; //is a reference with partitions

	private boolean implAuth = true;
	private boolean implType = true;
	private boolean implSection = true;
	

	private void reset(boolean full) {
	
		if(full) {
		
			auth = false;
			type = false;
			sect = false;
			subj = false;
			alias = false;
			
			isPart = false;
			
			context = new StringBuilder();
			
			offset = 0; 
			length = yylength(); 
		}
		
		date = false;
		num = false;
		caseNum = false;
		altNum = false;
		appl = false;
		def = false;
		part = false;
		ecli = false;
		
		implAuth = true;
		implType = true;
		implSection = true;
		
		pattern = new StringBuilder();
		buffer = new StringBuilder();		
	}
	
	private void checkAndSave() {
		
		checkAndSave(false);
	}
	
	private void checkAndSave(boolean last) {
	
		if( (auth && date) || (auth && num) || (auth && caseNum) || (auth && altNum) ||
			(type && date) || (type && num) || (type && caseNum) || (type && altNum) || 
			(alias) || (isPart && part) ) {

			if(part) {
				isPart = true;
			}

			String patternStr = pattern.toString().substring(0, pattern.length() - buffer.length());
			
			//addReference(patternStr, context.toString());
			addReference(patternStr, patternContext);
			
			addText(buffer.toString());
			
			reset(false);
			
			return;	
		}
		
		if(last || date || num || num || caseNum || altNum || appl || def || part || ecli) {
				
			addText(pattern.toString());
			
			reset(false);
		}
	}		
	
%} 


/* Generic patterns for case-law references. */


FEATURE = {BE_LEGAL_TYPE}|{BE_CASELAW_TYPE}|{BE_SUBJECT}|{BE_CASELAW_AUTH}|{BE_SECTION}|{BE_DATE}|{BE_NUMBER}|{BE_NUMBERYEAR}|{BE_CASENUMBER}|{BE_ALTNUMBER}|{BE_APPL}|{BE_DEF}|{BE_VERSUS}|{BE_GEO}|{BE_PART}|{BE_ECLI}|{BE_MISC}|{BE_CASELAWALIAS}|{BE_IDENTIFIER}

PATTERN = {FEATURE}({BE_SEP}?{FEATURE})+


/* Legislation Patterns to avoid */

LEG_PATTERN_1 = {BE_LEGISLATION_TYPE}{BE_SEP}?{BE_NUMBERYEAR}({BE_SEP}?{BE_DATE})?
LEG_PATTERN_2 = {BE_LEGISLATION_TYPE}{BE_SEP}?{BE_NUMBER}{BE_SEP}?{BE_DATE}
LEG_PATTERN_3 = {BE_LEGISLATION_TYPE}{BE_SEP}?{BE_DATE}{BE_SEP}?{BE_NUMBER}
LEG_PATTERN_4 = {BE_LEGISLATION_TYPE}{BE_SEP}?{BE_DATE}{BE_SEP}?{BE_NUMBERYEAR}

LEG_PATTERNS = {LEG_PATTERN_1}|{LEG_PATTERN_2}|{LEG_PATTERN_3}|{LEG_PATTERN_4}


%x readPattern  

%% 

{LEG_PATTERNS}	{ addText(yytext()); }

{PATTERN}		{ reset(true); patternContext = yytext(); yypushback(length); yybegin(readPattern); }


<readPattern> {

	
	{BE_CASELAW_TYPE}		{ offset += yylength(); if(type && !implType) { checkAndSave(); } type = true; implType = false; alias = false; pattern.append(yytext()); context.append(yytext()); buffer = new StringBuilder(); }
	{BE_LEGAL_TYPE}			{ offset += yylength(); if(type && !implType) { checkAndSave(); } type = true; implType = false; alias = false; pattern.append(yytext()); context.append(yytext()); buffer = new StringBuilder(); }

	{BE_CASELAW_AUTH}		{ offset += yylength(); if(auth && !implAuth) { checkAndSave(); context = new StringBuilder(); } auth = true; implAuth = false; alias = false; pattern.append(yytext()); context.append(yytext()); buffer = new StringBuilder();}
	
	{BE_SECTION}			{ offset += yylength(); if(sect && !implSection) { checkAndSave(); } sect = true; implSection = false; alias = false; pattern.append(yytext()); context.append(yytext()); buffer = new StringBuilder(); }
		
	{BE_SUBJECT}			{ offset += yylength(); if(subj) { checkAndSave(); } subj = true; alias = false; pattern.append(yytext()); context.append(yytext()); buffer = new StringBuilder(); }	
	
	{BE_DATE}				{ offset += yylength(); if(date) { checkAndSave(); } date = true; alias = false; pattern.append(yytext()); context.append(yytext()); buffer = new StringBuilder(); }
	
	{BE_NUMBER}				{ offset += yylength(); if(num) { checkAndSave(); } num = true; alias = false; pattern.append(yytext()); context.append(yytext()); buffer = new StringBuilder(); }

	{BE_NUMBERYEAR}			{ offset += yylength(); if(num) { checkAndSave(); } num = true; alias = false; pattern.append(yytext()); context.append(yytext()); buffer = new StringBuilder(); }		
	
	{BE_CASENUMBER}			{ offset += yylength(); if(caseNum) { checkAndSave(); } caseNum = true; alias = false; pattern.append(yytext()); context.append(yytext()); buffer = new StringBuilder(); }
	
	{BE_ALTNUMBER}			{ offset += yylength(); if(altNum) { checkAndSave(); } altNum = true; alias = false; pattern.append(yytext()); context.append(yytext()); buffer = new StringBuilder(); }

	{BE_APPL}				{ offset += yylength(); if(appl) { checkAndSave(); } appl = true; alias = false; pattern.append(yytext()); context.append(yytext()); buffer = new StringBuilder(); }

	{BE_DEF}				{ offset += yylength(); if(def) { checkAndSave(); } def = true; alias = false; pattern.append(yytext()); context.append(yytext()); buffer = new StringBuilder(); }

	{BE_VERSUS}				{ offset += yylength(); alias = false; pattern.append(yytext()); context.append(yytext()); buffer = new StringBuilder(); } 
	
	{BE_GEO}				{ offset += yylength(); alias = false; pattern.append(yytext()); context.append(yytext()); buffer = new StringBuilder(); }
	
	{BE_ECLI}				{ offset += yylength(); if(ecli) { checkAndSave(); } ecli = true; alias = false; pattern.append(yytext()); context.append(yytext()); buffer = new StringBuilder(); }		

	{BE_CASELAWALIAS}		{ offset += yylength(); alias = true; pattern.append(yytext()); context.append(yytext()); buffer = new StringBuilder(); }
							
	{BE_PART}				{ offset += yylength(); if(part) { checkAndSave(); } part = true; pattern.append(yytext()); context.append(yytext()); buffer = new StringBuilder(); }
	
	{BE_MISC}				{ offset += yylength(); pattern.append(yytext()); context.append(yytext()); buffer = new StringBuilder(); }

	
	[^] {
	
		offset++;
		
		if(offset >= length) {
			
			checkAndSave(true);
			
			if(offset > length) yypushback(1);
			
			yybegin(YYINITIAL);
		}
		
		pattern.append(yytext());
		context.append(yytext());
		buffer.append(yytext());
	}	
			
}



[^]		{ 
			addText(yytext()); 
		}



