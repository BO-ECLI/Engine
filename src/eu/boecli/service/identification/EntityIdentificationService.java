/*******************************************************************************
 * Copyright (c) 2016-2017 Institute of Legal Information Theory and Techniques (ITTIG/CNR)
 *
 * This program and the accompanying materials  are made available under the terms of the EUPL license v1.1 
 * or - as soon they will be approved by the European Commission -  subsequent versions of the EUPL (the "Licence"); 
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12 
 * Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on 
 * an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 *
 * Authors: Lorenzo Bacci (ITTIG/CNR)
 *******************************************************************************/
package eu.boecli.service.identification;

import java.util.ArrayList;

import eu.boecli.common.Alias;
import eu.boecli.common.CaseLawAlias;
import eu.boecli.common.CaseLawAuthority;
import eu.boecli.common.CaseLawType;
import eu.boecli.common.Entity;
import eu.boecli.common.Identifier;
import eu.boecli.common.LegalType;
import eu.boecli.common.LegislationAlias;
import eu.boecli.common.LegislationAuthority;
import eu.boecli.common.LegislationType;
import eu.boecli.common.Subject;
import eu.boecli.common.Type;
import eu.boecli.engine.EngineDocument;
import eu.boecli.service.AnnotationService;
import eu.boecli.service.impl.Util;

public abstract class EntityIdentificationService extends AnnotationService {
	
	/*
	 * A generic entity identification service.
	 * Provides auxiliary methods for the annotation of relevant textual entities.
	 */
	
	protected final void addIdentifier(Identifier identifier, String text) {
		
		//TODO syntax check on 'text'
		
		addText(OBE + Entity.IDENTIFIER + S + identifier + C + Util.removeAllAnnotations(text) + CBE);
	}
	
	protected final void addAlias(Alias alias, String text) {
		
		//TODO syntax check on 'text'
		
		if(alias instanceof CaseLawAlias) {
			
			addText(OBE + Entity.CASELAW_ALIAS + S + alias + C + text + CBE);
			return;
		}
		
		if(alias instanceof LegislationAlias) {
			
			addText(OBE + Entity.LEGISLATION_ALIAS + S + alias + C + text + CBE);
			return;
		}
		
		((EngineDocument) getEngineDocument()).addMessage("Alias value not allowed: " + alias + "(text: " + text + ")");
		((EngineDocument) getEngineDocument()).addMessage("An alias value should implement either CaseLawAlias or LegislationAlias.");
		addText(text);
	}

	protected final void addAuthority(CaseLawAuthority authority, String text) {
		
		//TODO syntax check on 'text'
		
		addText(OBE + Entity.CASELAW_AUTHORITY + S + authority + C + Util.removeAllAnnotations(text) + CBE);
	}
	
	protected final void addAuthority(LegislationAuthority authority, String text) {
		
		//TODO syntax check on 'text'
		
		addText(OBE + Entity.LEGISLATION_AUTHORITY + S + authority + C + Util.removeAllAnnotations(text) + CBE);
	}
		
	protected final void addType(Type type, String text) {
		
		//TODO syntax check on 'text'
		
		if(type instanceof CaseLawType) {
			
			addText(OBE + Entity.CASELAW_TYPE + S + type + C + text + CBE);
			return;
		}
		
		if(type instanceof LegislationType) {
			
			addText(OBE + Entity.LEGISLATION_TYPE + S + type + C + text + CBE);
			return;
		}
		
		if(type instanceof LegalType) {
			
			addText(OBE + Entity.LEGAL_TYPE + S + type + C + text + CBE);
			return;
		}
		
		((EngineDocument) getEngineDocument()).addMessage("Type value not allowed: " + type + "(text: " + text + ")");
		((EngineDocument) getEngineDocument()).addMessage("A type value should implement either CaseLawType or LegislationType or LegalType.");
		addText(text);

	}
	
	protected final void addSubject(Subject subject, String text) {
		
		//TODO syntax check on 'text'
		
		addText(OBE + Entity.SUBJECT + S + subject + C + text + CBE);
	}	
	
	protected final void addSection(String value, String text) {
		
		//TODO syntax check on 'text'
		
		value = value.trim();
		
		if(value.equals("")) {
			
			System.err.println("addSection() - empty value.");
			addText(text);
			return;	
		}

		
		addText(OBE + Entity.SECTION + S + value + C + Util.removeAllAnnotations(text) + CBE);
	}

	protected final void addDate(String day, String month, String text) {
		
		addDate(day, month, "", text);
	}
	
	protected final void addDate(String day, String month, String year, String text) {
		
		//TODO syntax check on 'text'
		
		day = day.trim();
		month = month.trim();
		year = year.trim();
		
		if( !checkDay(day) || !checkMonth(month) || ( !year.equals("") && !checkYear(year)) ) {
			
			System.err.println("addDate() - bad values (day:\"" + day + "\" month:\"" + month + "\" year:\"" + year + "\")");
			addText(text);
			return;
		}
		
		if(day.length() == 1) {
			
			day = "0" + day;
		}
		
		if(month.length() == 1) {
			
			month = "0" + month;
		}
		
		String normalizedDate = month + "-" + day;
		
		if( !year.equals("")) {
			
			year = Util.normalizeYear(year);
			
			normalizedDate = year + "-" + normalizedDate; 
		}
		
		addText(OBE + Entity.DATE + S + normalizedDate + C + text + CBE);
	}
	
	protected final void addNumber(String number, String text) {
	
		addNumber("", number, "", text);
	}
	
	protected final void addNumber(String number, String year, String text) {
		
		addNumber("", number, year, text);
	}
	
	protected final void addNumber(String prefix, String number, String year, String text) {
		
		//TODO syntax check on 'text'

		prefix = Util.removeAllAnnotations(prefix.trim());
		number = number.trim();
		year = year.trim();
		
		if(number.equals("")) {
			
			System.err.println("addNumber() - empty number.");
			addText(text);
			return;	
		}

		if( !prefix.equals("")) {
			
			prefix = prefix.toUpperCase() + "-";  //by convention, not always true to the original form
		}
		
		if( !year.equals("")) {
			
			if( !checkYear(year)) {
				
				System.err.println("addNumber() - year not valid: " + year);
				addText(text);
				return;	
			}
			
			year = "/" + year;
		}
		
		String value = prefix + number + year;
		
		addText(OBE + Entity.NUMBER + S + value + C + Util.removeAllAnnotations(text) + CBE);
	}
	
	
	protected final void addAltNumber(String number, String text) {
		
		addAltNumber("", number, "", text);
	}
	
	protected final void addAltNumber(String number, String year, String text) {
		
		addAltNumber("", number, year, text);
	}
	
	protected final void addAltNumber(String prefix, String number, String year, String text) {
		
		//TODO syntax check on 'text'

		prefix = Util.removeAllAnnotations(prefix.trim());
		number = number.trim();
		year = year.trim();
		
		if(number.equals("")) {
			
			System.err.println("addAltNumber() - empty number.");
			addText(text);
			return;	
		}

		if( !prefix.equals("")) {
			
			prefix = prefix.toUpperCase() + "-";  //by convention, not always true to the original form
		}
		
		if( !year.equals("")) {
			
			if( !checkYear(year)) {
				
				System.err.println("addAltNumber() - year not valid: " + year);
				addText(text);
				return;	
			}
			
			year = "/" + year;
		}
		
		String value = prefix + number + year;
		
		addText(OBE + Entity.ALT_NUMBER + S + value + C + Util.removeAllAnnotations(text) + CBE);
	}
	
	protected final void addCaseNumber(String number, String text) {
		
		addCaseNumber("", number, "", text);
	}
	
	protected final void addCaseNumber(String number, String year, String text) {
		
		addCaseNumber("", number, year, text);
	}
	
	protected final void addCaseNumber(String prefix, String number, String year, String text) {
		
		//TODO syntax check on 'text'

		prefix = Util.removeAllAnnotations(prefix.trim());
		number = number.trim();
		year = year.trim();
		
		if(number.length() < 1) {
			
			System.err.println("addCaseNumber() - empty number.");
			addText(text);
			return;	
		}

		if( !prefix.equals("")) {
			
			prefix = prefix.toUpperCase() + "-"; //follows the CJEU convention
		}
		
		if( !year.equals("")) {
			
			if( !checkYear(year)) {
				
				System.err.println("addCaseNumber() - year not valid: " + year);
				addText(text);
				return;	
			}
			
			year = "/" + year;
		}
		
		String value = prefix + number + year;
		
		addText(OBE + Entity.CASE_NUMBER + S + value + C + Util.removeAllAnnotations(text) + CBE);
	}
	
	protected final void addJointCaseNumbers(ArrayList<String> numbers, String text) {
		
		addJointCaseNumbers(null, numbers, null, text);
	}
	
	protected final void addJointCaseNumbers(ArrayList<String> numbers, ArrayList<String> years, String text) {
		
		addJointCaseNumbers(null, numbers, years, text);
	}
	
	protected final void addJointCaseNumbers(ArrayList<String> prefixes, ArrayList<String> numbers, ArrayList<String> years, String text) {
		
		if(numbers == null || numbers.size() == 0) {
			
			System.err.println("addJointCaseNumbers() - empty numbers.");
			addText(text);
			return;	
		}
		
		String value = "";
		
		for(int i = 0; i < numbers.size(); i++) {
			
			String number = numbers.get(i).trim();
			String prefix = "";
			String year = "";
			
			if(number.length() < 1) {
				
				System.err.println("addJointCaseNumbers() - empty number.");
				addText(text);
				return;	
			}
			
			if(prefixes != null) {
				
				for(int p = 0; p < prefixes.size(); p++) {
					
					if(p == i) {
						prefix = prefixes.get(p).trim();
						break;
					}
				}
			}
			
			if(years != null) {
				
				for(int y = 0; y < years.size(); y++) {
					
					if(y == i) {
						year = years.get(y).trim();
						break;
					}
				}
			}
			
			if( !prefix.equals("")) {
				
				prefix = prefix.toUpperCase() + "-";
			}
			
			if( !year.equals("")) {
				
				if( !checkYear(year)) {
					
					System.err.println("addJointCaseNumbers() - year not valid: " + year);
					addText(text);
					return;	
				}
				
				if(year.length() == 4) {
					
					year = year.substring(2);
				}

				year = "/" + year;
			}
			
			if( !value.equals("")) {
				
				value += ";";
			}
			
			value += prefix + number + year; 
		}
		
		addText(OBE + Entity.CASE_NUMBER + S + value + C + Util.removeAllAnnotations(text) + CBE);
	}
	
	protected final void addApplicant(String text) {
		
		//TODO syntax check on 'text'
		
		addText(OBE + Entity.APPLICANT + C + Util.removeAllAnnotations(text) + CBE);
	}

	protected final void addDefendant(String text) {
		
		//TODO syntax check on 'text'
		
		addText(OBE + Entity.DEFENDANT + C + Util.removeAllAnnotations(text) + CBE);
	}

	protected final void addDefendant(String value, String text) {
		
		//TODO syntax check on 'text'
		
		value = value.trim().toUpperCase();
		
		if(value.equals("")) {
			
			addDefendant(text);
			return;
		}
		
		addText(OBE + Entity.DEFENDANT + S + value + C + Util.removeAllAnnotations(text) + CBE);
	}

	protected final void addVersus(String text) {
		
		//TODO syntax check on 'text'
		
		addText(OBE + Entity.VERSUS + C + text + CBE);
	}

	@Deprecated
	protected final void addPartition(String value, String text) {
		
		//TODO syntax check on 'text'
		
		value = value.trim();
		
		String textWithoutAnnotations = Util.removeAllAnnotations(text);
		
		addText(OBE + Entity.PARTITION + S + value + C + textWithoutAnnotations + CBE);
	}
	
	protected final void addPartition(String article, String paragraph, String letter, String item, String text) {
		
		String value = "";
		
		if( !article.trim().equals("")) {
			value += "article-" + article + ";";
		}

		if( !paragraph.trim().equals("")) {
			value += "paragraph-" + paragraph + ";";
		}

		if( !letter.trim().equals("")) {
			value += "letter-" + letter + ";";
		}

		if( !item.trim().equals("")) {
			value += "item-" + item + ";";
		}
		
		if(value.equals("")) {
			
			addText(text);
			return;
		}
		
		addText(OBE + Entity.PARTITION + S + value + C + Util.removeAllAnnotations(text) + CBE);
	}
	
	private void addEntity(Entity entity, String value, String latin, String text) {
		
		//TODO syntax check on 'value' (number or letter)
		//TODO syntax check on 'latin'
		//TODO syntax check on 'text'
		
		if(latin.trim().length() == 0) {
			addText(OBE + entity + S + value + C + text + CBE);
		} else {
			addText(OBE + entity + S + value + "-" + latin.toLowerCase() + C + text + CBE);
		}		
	}
	
	protected final void addArticle(String number, String text) {
		
		number = number.trim();
		
		addEntity(Entity.ARTICLE, number, "", text);
	}
	
	protected final void addArticle(String number, String latin, String text) {
		
		number = number.trim();
		latin = latin.trim();
		
		addEntity(Entity.ARTICLE, number, latin, text);
	}
		
	protected final void addParagraph(String number, String text) {
		
		number = number.trim();
		
		addEntity(Entity.PARAGRAPH, number, "", text);
	}
	
	protected final void addParagraph(String number, String latin, String text) {
		
		number = number.trim();
		latin = latin.trim();
		
		addEntity(Entity.PARAGRAPH, number, latin, text);
	}
	
	protected final void addLetter(String letter, String text) {
		
		letter = letter.trim();
		
		addEntity(Entity.LETTER, letter, "", text);
	}
	
	protected final void addLetter(String letter, String latin, String text) {
		
		letter = letter.trim();
		latin = latin.trim();
		
		addEntity(Entity.LETTER, letter, latin, text);	
	}
	
	protected final void addItem(String number, String text) {
		
		number = number.trim();
		
		addEntity(Entity.ITEM, number, "", Util.removeAllAnnotations(text));
	}
	
	protected final void addItem(String number, String latin, String text) {
		
		number = number.trim();
		latin = latin.trim();
		
		addEntity(Entity.ITEM, number, latin, Util.removeAllAnnotations(text));	
	}

	protected final void addGeographic(String value, String text) {
		
		//TODO syntax check on 'text'
		
		value = value.trim().toUpperCase();
		
		if(value.equals("")) {
			
			addText(text);
			return;
		}
		
		addText(OBE + Entity.GEO + S + value + C + text + CBE);
	}

	protected final void addMisc(String text) {

		//TODO syntax check on 'text'
		
		addText(OBE + Entity.MISC + C + Util.removeAllAnnotations(text) + CBE);
	}
	
	protected final void addStopword(String text) {

		//TODO syntax check on 'text'
		
		addText(OBE + Entity.STOPWORD + C + text + CBE);
	}
	
	protected final void addStopword(String value, String text) {
		
		//TODO syntax check on 'value' (AND, OF, WITH, BY, DET, etc.)
		//TODO syntax check on 'text'
		
		value = value.trim().toUpperCase();
		
		if(value.equals("")) {
			
			addStopword(text);
			return;
		}
		
		addText(OBE + Entity.STOPWORD + S + value + C + text + CBE);
	}

	protected final void addIgnore(String text) {
		
		//TODO syntax check on 'text'
		
		addText(OBE + Entity.IGNORE + C + text + CBE);
	}	
	
	
	
	private static boolean checkDay(String day) {
		
		if(day.length() != 1 && day.length() != 2) {
			return false;
		}
		
		int value = 0;
		
		try {
			
			value = Integer.valueOf(day);
			
		} catch (NumberFormatException e) {

			return false;
		}
	
		if(value < 1 || value > 31) {
			
			return false;
		}
		
		return true;
	}

	private static boolean checkMonth(String month) {
		
		if(month.length() != 1 && month.length() != 2) {
			return false;
		}
		
		int value = 0;
		
		try {
			
			value = Integer.valueOf(month);
			
		} catch (NumberFormatException e) {

			return false;
		}
	
		if(value < 1 || value > 12) {
			
			return false;
		}
		
		return true;
	}

	private static boolean checkYear(String year) {
		
		if(year.length() != 2 && year.length() != 4) {
			return false;
		}
		
		int value = 0;
		
		try {
			
			value = Integer.valueOf(year);
			
		} catch (NumberFormatException e) {

			return false;
		}
		
		if(year.length() == 4) {
			
			if(value < 1860 || value > 2020) {
				
				return false;
			}
		}
		
		return true;
	}
	
}
